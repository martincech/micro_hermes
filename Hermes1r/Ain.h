//*****************************************************************************
//
//    Ain.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ain_H__
   #define __Ain_H__

#include "Hardware.h"     // pro podminenou kompilaci


void AinInit();
  // Inicializuje AD prevodnik

byte AinRead(byte Channel);
  // Precte analogovy vstup, vstup muze byt 0 az AIN_COUNT-1

byte AinCalibrate(byte Channel);
  // Nacte kalibraci vstupu Channel

#endif
