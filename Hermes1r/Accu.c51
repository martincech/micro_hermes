//*****************************************************************************
//
//    Accu.c - Kontrola napajeciho napeti
//    Version 1.0
//
//*****************************************************************************

#include "Accu.h"
#include "..\Hermes1\Hermes1.h"
#include "..\inc\System.h"     // "operacni system" - kvuli SysDelay()

#include "Ain.h"               // Analogove vstupy
#include "Cal.h"               // Kalibrace

//-----------------------------------------------------------------------------
// Globalni promenne
//-----------------------------------------------------------------------------

TAccu __xdata__ Accu;

//-----------------------------------------------------------------------------
// Lokalni promenne
//-----------------------------------------------------------------------------

#define READ_PERIOD     60                      // Stav nacitam kazdych READ_PERIOD sekund
static byte __xdata__   Counter;                // Odpocet pro nacitani stavu aku

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AccuInit() {
  Accu.Indication = 0;
  Accu.Busy       = NO;
  Counter         = READ_PERIOD;                // Chci, aby hned v prvnim kroku pozapnuti nacetl
}

//-----------------------------------------------------------------------------
// Nacteni stavu akumulatoru
//-----------------------------------------------------------------------------

void AccuRead() {
  // Nacte stav akumulatoru
  if (++Counter < READ_PERIOD) {
    return;                     // V tomto kroku nenacitam
  }
  Counter         = 0;          // Zacinam zase od nuly
  Accu.Busy       = YES;
  Accu.Conversion = AinRead(AIN_ACCU);
  Accu.Busy       = NO;
  Accu.Indication = CalCalculate(Accu.Conversion, Calibration.AccuMin, Calibration.AccuMax, 0, ACCU_INDICATION_MAX);
}

//-----------------------------------------------------------------------------
// Kalibrace akumulatoru
//-----------------------------------------------------------------------------

byte AccuCalibrate() {
  // Nacte kalibraci akumulatoru
  byte Conversion;

  Accu.Busy  = YES;
  Conversion = AinCalibrate(AIN_ACCU);
  Accu.Busy  = NO;
  return Conversion;
}
