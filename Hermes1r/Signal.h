//*****************************************************************************
//
//    Signal.h - Mereni sily signalu
//    Version 1.0
//
//*****************************************************************************

#ifndef __Signal_H__
   #define __Signal_H__

#include "Hardware.h"     // zakladni datove typy

#define SIGNAL_INDICATION_MAX   4     // Maximalni hodnota poctu dilku indikace

typedef struct {
  byte          Conversion;             // Prevod
  byte          Indication;             // Pocet dilku
  byte          IndicatedConversion;    // Prevod odpovidajici Indication
  byte          ZeroSignalIgnored;      // YES/NO objevil se nulovy signal, ktery jsem ignoroval
} TSignal;
extern TSignal __xdata__ Signal;

void SignalInit(void);

void SignalExecute(void);
// Obsluha mereni sily signalu. Volat 1x za sekundu.

byte SignalCalibrate(void);
  // Nacte kalibraci sily signalu

void SignalPacketValid(void);
  // Oznameni prijeti platneho paketu

#endif
