//*****************************************************************************
//
//    X9313.h  -  X9313 services
//    Verze 1.0
//
//*****************************************************************************

#ifndef __X9313_H__
   #define __X9313_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void E2PotDown();
  // Snizi odpor potenciometru o 1 krok

void E2PotUp();
  // Zvysi odpor potenciometru o 1 krok

#endif

