//*****************************************************************************
//
//   At45dbxx.h     Flash memory AT45DBxxx
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __At45dbxx_H__
   #define __At45dbxx_H__

#ifndef __Spi_H__
   #include "..\inc\Spi.h"
#endif

// velikost stranky a pocet stranek :

#if defined(__AT45DB161__) || defined(__AT45DB321__)
   #define FLASH_PAGE_SIZE     528

   #ifdef __AT45DB161__
      #define FLASH_PAGES          4096 // pocet stranek
      #define FLASH_SIGNATURE      0x2C
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB321__
      #define FLASH_PAGES          8192 // pocet stranek
      #define FLASH_SIGNATURE      0x34
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
#elif defined(__AT45DB081__) || defined(__AT45DB041__) || defined(__AT45DB021__) || defined(__AT45DB011__)
   #define FLASH_PAGE_SIZE     264

   #ifdef __AT45DB011__
      #define FLASH_PAGES          512  // pocet stranek
      #define FLASH_SIGNATURE      0x0C
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB021__
      #define FLASH_PAGES          1024 // pocet stranek
      #define FLASH_SIGNATURE      0x14
      #define FLASH_SIGNATURE_MASK 0x3C
   #endif
   #ifdef __AT45DB041__
      #define FLASH_PAGES          2048 // pocet stranek
      #define FLASH_SIGNATURE      0x18
      #define FLASH_SIGNATURE_MASK 0x38
   #endif
   #ifdef __AT45DB081__
      #define FLASH_PAGES          4096 // pocet stranek
      #define FLASH_SIGNATURE      0x20
      #define FLASH_SIGNATURE_MASK 0x38
   #endif
#elif defined(__AT45DB642__)
   #define FLASH_PAGE_SIZE 1056
   #define FLASH_PAGES     8192
#else
   #error "Unknown FLASH DATA device"
#endif

#define FlashCheckSignature( Status)    (((Status) & FLASH_SIGNATURE_MASK) == FLASH_SIGNATURE)

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

// Inicializace viz Spi

byte FlashStatus( void);
// Vrati stavovy byte pameti

TYesNo FlashWaitForReady( void);
// Ceka na dokonceni operace

//------- Zapis do bufferu :

void FlashWriteBufferStart( word Offset);
// Zahaji sekvencni zapis do bufferu

#define FlashWriteBufferData( Value) SpiWriteByte( Value)
// Zapis na aktualni adresu a posun adresy

#define FlashWriteBufferDone() SpiRelease()
// Ukonceni zapisu

//------- Ukladani bufferu :

void FlashSaveBuffer( word Page);
// Zapis bufferu do flash

void FlashLoadBuffer( word Page);
// Plneni bufferu z flash

//------- Spolecne funkce pro cteni (buffer/array) :

#define FlashCommonReadData() SpiReadByte()
// Cteni z aktualni adresy a posun adresy

#define FlashCommonReadDone() SpiRelease()
// Ukonceni cteni

//------- Cteni primo z memory array :

void FlashBlockReadStart( word Page, word Offset);
// Zahaji sekvencni cteni dat primo z flash

#define FlashBlockReadData() FlashCommonReadData()
// Cteni z aktualni adresy a posun adresy

#define FlashBlockReadDone() FlashCommonReadDone()
// Ukonceni cteni

//------- Cteni z bufferu :

void FlashBufferReadStart( word Offset);
// Zahaji sekvencni cteni dat z bufferu

#define FlashBufferReadData() FlashCommonReadData()
// Cteni z bufferu a posun adresy

#define FlashBufferReadDone() FlashCommonReadDone()
// Ukonceni cteni bufferu


#endif
