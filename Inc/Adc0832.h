//*****************************************************************************
//
//    Adc0832.h  -  ADC 0832 services
//    Version 1.0
//
//*****************************************************************************

#ifndef __Adc0832_H__
   #define __Adc0832_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Konstanty pro adresaci kanalu :

// Pro interni pouziti :

#define ADC_DIFF   0x01           // SGL/DIF - diff vcetne startbitu
#define ADC_SINGLE 0x03           // SGL/DIF - single vcetne startbitu
#define ADC_ODD    0x04           // ODD/SIGN

// Pro uzivatele API :

#define ADC_SINGLE_CH0   (ADC_SINGLE)                                    // kanal 0
#define ADC_SINGLE_CH1   (ADC_SINGLE | ADC_ODD)                          // kanal 1

#define ADC_DIFF_CH0     (ADC_DIFF)                                      // kanal 0,1

#define ADC_DIFF_SWAP    ADC_ODD     // prehozeni +/- vstupu u diff, nizsi je -, ORovat s adresou

#define ADC_RANGE        256         // pocet kroku prevodniku. Rozsah je 0..ADC_RANGE-1


void AdcInit( void);
// Inicializuje sbernice

byte AdcRead( byte channel);
// Adresuje kanal <channel>, vrati prectenou hodnotu

#endif

