//*****************************************************************************
//
//   MBPkt.h     Modbus serial communication packets
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBPkt_H__
   #define __MBPkt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Rozsahy adres :
#define MB_BROADCAST_ADDRESS    0
#define MB_MIN_ADDRESS          1
#define MB_MAX_ADDRESS        247
// rezervovano :              248..255


// delkova omezeni :
#define MB_PDU_FUNCTION_SIZE    1      // byte
#define MB_PDU_MAX_DATA_SIZE  252      // bytes
#define MB_MAX_PDU_SIZE       (MB_PDU_FUNCTION_SIZE + MB_PDU_MAX_DATA_SIZE)

// RTU - binarni paket :
// address (byte) PDU (variable) CRC ( word)

#define MB_RTU_ADDRESS_SIZE     1      // byte
#define MB_RTU_CRC_SIZE         2      // word
#define MB_RTU_MIN_SIZE         (MB_RTU_ADDRESS_SIZE + MB_RTU_CRC_SIZE)
#define MB_RTU_MAX_SIZE         (MB_RTU_MIN_SIZE + MB_MAX_PDU_SIZE)

// ASCII - textovy paket :
// ':' address (2 char) PDU (char touples) LRC (2 chars) '<CR>''<LF>'

#define MB_ASCII_FRAME_SIZE     3      // leading/trailing chars
#define MB_ASCII_LEADING_CHAR   ':'    // uvodni znak
#define MB_ASCII_TRAILER1_CHAR  '\r'   // predposledni znak
#define MB_ASCII_TRAILER2_CHAR  '\n'   // posledni znak

#define MB_ASCII_ADDRESS_SIZE   1      // byte
#define MB_ASCII_LRC_SIZE       1      // byte

#define MB_ASCII_MIN_SIZE       (MB_ASCII_FRAME_SIZE + 2 * MB_ASCII_ADDRESS_SIZE + 2 * MB_ASCII_LRC_SIZE) // minimalni ramec
#define MB_ASCII_MAX_SIZE       (MB_ASCII_MIN_SIZE + 2 * MB_MAX_PDU_SIZE)

#endif
