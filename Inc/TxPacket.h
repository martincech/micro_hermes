//*****************************************************************************
//
//    TxPacket.c  - Packet transmitter
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __TxPacket_H__
   #define __TxPacket_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


// Globalni data :

extern TPacket __xdata__ Packet;
extern bit _PacketTxActive;

void TxPacketInit( void);
// Inicializace komunikace

void TxPacketTransmit( void);
// Paket pripraven, vysilej

void TxPacketStop( void);
// Uplne zastaveni vysilani

#define TxPacketTransmitted()   (!_PacketTxActive)
// Paket odvysilan

#endif
