//*****************************************************************************
//
//    GsmCtl.h     GSM controller
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmCtl_H__
   #define __GsmCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Kontextove stavy :

typedef enum {
   GSM_IDLE,                           // zadna cinnost
   GSM_POWERED,                        // zapnuto, neni spojeni
   GSM_WAIT_POWERED,                   // ceka se na nabeh modemu
   GSM_CHECK,                          // kontrola spojeni
   GSM_WAIT_FOR_CALL,                  // cekani na prichozi hovor
   GSM_WAIT_FOR_CONNECT,               // cekani na datove spojeni
   GSM_DATA_MODE,                      // rezim prenosu dat
   GSM_USER_MODE,                      // rizeni prebral uzivatel
   GSM_SMS_SENDING                     // cekani na potvrzeni odeslani SMS
} TGsmStatus;

typedef struct {
  byte Status;           // kontext
  byte Counter;          // kontextovy citac
  byte Registered;       // YES/NO modul je prihlasen do site (nejde vyuzit primo GsmStatus, ten se i behem prihlaseni meni)
} TGsmCtl;
extern TGsmCtl __xdata__ GsmCtl;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void GsmStart();
// Zahajeni cinnosti

void GsmStop();
// Ukonceni cinnosti

void GsmExecute( void);
// Zpracovava udalosti GSM (volat 1x za sekundu)

#define GsmReady()            (GsmCtl.Status == GSM_WAIT_FOR_CALL)
// Je v prikazovem rezimu, lze vykonavat prikazy

#define GsmCalling()          (GsmCtl.Status == GSM_DATA_MODE)
// Prave se vola

#define GsmIsUserMode()       (GsmCtl.Status == GSM_USER_MODE)
// Je v uzivatelskem rezimu

#define GsmSetUserMode()       GsmCtl.Status = GSM_USER_MODE
// Prepnuti do uzivatelskeho rezimu

#define GsmReturnToDataMode()  GsmCtl.Status = GSM_DATA_MODE; GsmCtl.Counter = GSM_HANGUP_TIMEOUT
// Navrat z uzivatelskeho do datoveho modu

//-----------------------------------------------------------------------------
// Callback
//-----------------------------------------------------------------------------

void GsmConnected();
// Provede potrebne kroky po pripojeni datoveho volani

void GsmDisconnected();
// Provede potrebne kroky po odpojeni datoveho volani

void GsmExecuteCommand( byte Cmd, dword Data);
// Vyhodnoceni prikazu a odpoved

void GsmExecuteUser( void);
// Funkce pro uzivatelske zpracovani dat

void GsmSmsSendTimeout( void);
// Timeout pri odesilani SMS

void GsmSmsSendOk( void);
// Uspesne odeslani SMS

#endif

