//*****************************************************************************
//
//    Ad421.h  -  AD421 services
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Ad421_H__
   #define __Ad421_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void DacIInit();
  // Inicializuje proudovou smycku

void DacIWrite(unsigned int Hodnota);
// Zapise do prevodniku hodnotu Hodnota

#endif

