//*****************************************************************************
//
//   MBDef.h     Modbus common data definitions
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBDef_H__
   #define __MBDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#define COM_115200Bd  0                // smluvena baudova rychlost

// Parita :
typedef enum {
   COM_PARITY_NONE,                    // bez parity
   COM_PARITY_EVEN,                    // suda parita
   COM_PARITY_ODD,                     // licha parita
   COM_PARITY_MARK,                    // konstantni parita - 2 stopbity
   COM_PARITY_7BITS_EVEN,              // suda parita 7 bitu
   COM_PARITY_7BITS_ODD,               // licha parita 7 bitu
   COM_PARITY_7BITS_MARK,              // konstantni parita - 2 stopbity, 7 bitu
   _COM_PARITY_LAST
} TComParity;

// rezimy paketove urovne :
typedef enum {
   MB_ASCII_MODE,                    // ASCII mode
   MB_RTU_MODE,                      // RTU mode
   _MB_LAST_MODE
} TMBPacketMode;

#endif
