//*****************************************************************************
//
//    Bcd.h - BCD utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Bcd_H__
   #define __Bcd_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

//-----------------------------------------------------------------------------
// operace s nibbly :
//-----------------------------------------------------------------------------

#define lnibble( x)     ((byte)(x) & 0x0F)
#define hnibble( x)     ((byte)(x) >> 4)

//-----------------------------------------------------------------------------
// prevody do BCD :
//-----------------------------------------------------------------------------

word bbin2bcd( byte x);
// prevede 0..255 do bcd

dword wbin2bcd( word x);
// prevede 0..65535 do bcd

dword dbin2bcd( dword x);
// prevede 0..99 999 999 do bcd

//-----------------------------------------------------------------------------
// prevody z BCD :
//-----------------------------------------------------------------------------

byte bbcd2bin( byte x);
// prevede 0..0x99 do bin

word wbcd2bin( word x);
// prevede 0..0x9999 do bin

dword dbcd2bin( dword x);
// prevede 0..0x99 999 999 do bin

//-----------------------------------------------------------------------------
// BCD aritmetika
//-----------------------------------------------------------------------------

word bbcdinc( byte x);
// vrati x+1

dword wbcdinc( word x);
// vrati x+1

//-----------------------------------------------------------------------------

byte bbcddec( byte x);
// vrati x-1

word wbcddec( word x);
// vrati x-1

//-----------------------------------------------------------------------------

word bbcdadd( byte x, byte y);
// vrati x+y

dword wbcdadd( word x, word y);
// vrati x+y

//-----------------------------------------------------------------------------

byte bbcdsub( byte x, byte y);
// vrati x-y

word wbcdsub( word x, word y);
// vrati x-y

//-----------------------------------------------------------------------------
// Prevod na znaky :
//-----------------------------------------------------------------------------

#define nibble2dec( x)  (((x) & 0x0F) + '0')
//byte nibble2dec( byte x);
// prevede dolni nibble na '0'..'9'

byte nibble2hex( byte x);
// prevede dolni nibble na '0'..'F'

//-----------------------------------------------------------------------------
// Prevod na cislo :
//-----------------------------------------------------------------------------

#define char2dec( ch) ((ch) - '0')
//byte char2dec( byte ch);
// prevede ASCII '0'..'9' na cislo

#define char2hex( ch) ((ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10)
//byte char2hex( byte ch)
// prevede ASCII '0'..'9' a 'A'..'F' na cislo

#endif
