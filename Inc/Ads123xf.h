//*****************************************************************************
//
//    Ads123xf.h  -  A/D convertor ADS1230/ADS1232 services with filtering
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads123xf_H__
   #define __Ads123xf_H__

#include "Filter.h"

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

#define AdcIsReady()         (!AdcDRDY)
// Prevodnik ma pripravena data

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

int32 AdcReadValue( void);
// Precteni namerene hodnoty

//-----------------------------------------------------------------------------

// POZOR, od AdsStart do prvniho nastaveni AdcDataReady()
// uplyne 800ms + 100ms * n kde <n> je pocet prumerovanych vzorku

void AdcStart( void);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

TRawWeight AdcRawRead( void);
// Cteni okamzite hodnoty prevodu

// POZOR, nasledujici dve funkce volat pri zakazanem preruseni

#define AdcDataReady()   GetFilterReady()
// Test na nove ustalenou hodnotu

TRawWeight AdcRead( void);
// Cteni ustalene hodnoty (volat jen po AdcDataReady = true)
// Vrati rozdil ustalenych hodnot

//-----------------------------------------------------------------------------

#endif
