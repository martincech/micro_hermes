//*****************************************************************************
//
//    Tda8444.h  -  DAC TDA8444 convertor services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tda8444_H__
   #define __Tda8444_H__

#ifndef __Iic_H__
   #include "Iic.h"
#endif

#define DAC_COUNT        8    // pocet prevodniku, kanal je 0..DAC_COUNT-1
#define DAC_VALUE_MAX 0x3F    // rozsah hodnot prevodniku 0..DAC_VALUE_MAX vcetne

TYesNo DacInit( void);
// Inicializuje sbernici a prevodnik

TYesNo DacWrite( byte channel, byte value);
// Zapis do kanalu prevodniku

TYesNo DacBurstWrite( byte values[]);
// Zapise pole do vsech 8 kanalu prevodniku

#endif

