//*****************************************************************************
//
//    Servo.h - Servo control module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Servo_H__
   #define __Servo_H__

#ifndef __Adc0838_H__
   #include "Adc0838.h"
#endif

void ServoInit( void);
// Inicializuje modul serv

void ResetServoTimer(byte Cislo);
// Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu

void ServoCheck( void);
// Kontroluje cinnost serva. Volat 1x za sekundu

#endif
