//*****************************************************************************
//
//    conio.h - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
   #define __conio_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

char putchar( char c);
// standardni zapis znaku

void cputs( char code *string);
// vystup retezce <string> z kodove pameti

void cprinthex( dword x, byte width);
// vystup hexa cisla <x> o sirce <width> znaku

void cprintdec( int32 x, byte width);
// vystup dekadickeho cisla <x> o sirce <width> znaku

#define cbyte( x)  cprinthex( x, 2 | FMT_LEADING_0)
// Tiskne byte jako dve hexa cislice (pro BCD)

#define cword( x)  cprinthex( x, 4 | FMT_LEADING_0)
// Tiskne word jako ctyri hexa cislice (pro BCD)

#define cdword( x) cprinthex( x, 0 | FMT_LEADING_0)
// Tiskne dword jako osm hexa cislic (pro BCD)

#define cint8( x)   cprintdec( x, 3);
// Tiskne byte jako cislo se znamenkem

#define cint16( x)  cprintdec( x, 5);
// Tiskne int jako cislo se znamenkem

#define cint32( x)  cprintdec( x, 0);
// Tiskne long jako cislo se znamenkem
// (max 8 platnych cislic)

void cfloat( dword x, byte w, byte d);
// Tiskne BCD cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. Znamenko se zada jako <x> | PRT_MINUS

void cprintf( char code *format, ...);
// jednoduchy formatovany vystup


//------------------------------------------------------------------------------
#ifdef __SIMPLE_DISPLAY__

// mezera bez prepsani podkladu :
#define CH_NULL           0

// Interni promenne, nemodifikovat ! :
//extern byte xdata _row;
//extern byte xdata _col;
extern byte _row;
extern byte _col;


void DisplayInit( void);
// Inicializace zobrazeni

void DisplayCursor( byte on);
// je-li <on>=NO zhasne kurzor, jinak rozsviti

void DisplayClear( void);
// smaze display

#define DisplayGotoRC( r, c)  _row = (r); _col = (c)
// presune kurzor na zadanou textovou pozici <r> radek <c> sloupec

void DisplayClrEol( void);
// smaze az do konce radku, nemeni pozici kurzoru

char putchar( char c);
// standardni zapis znaku

#endif // __SIMPLE_DISPLAY__
//------------------------------------------------------------------------------

#endif
