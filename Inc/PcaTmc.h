//*****************************************************************************
//
//    PcaTmc.h  -  TMC project PCA services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Pca_H__
   #define __Pca_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Trocha hudby :

#define NOTE_C1    2616
#define NOTE_CIS1  2772
#define NOTE_D1    2937
#define NOTE_DIS1  3111
#define NOTE_E1    3296
#define NOTE_F1    3492
#define NOTE_FIS1  3700
#define NOTE_G1    3920
#define NOTE_GIS1  4153
#define NOTE_A1    4400
#define NOTE_AIS1  4661
#define NOTE_H1    4939

#define NOTE_C2    5233
#define NOTE_CIS2  5544
#define NOTE_D2    5873
#define NOTE_DIS2  6223
#define NOTE_E2    6593
#define NOTE_F2    6985
#define NOTE_FIS2  7400
#define NOTE_G2    7840
#define NOTE_GIS2  8306
#define NOTE_A2    8800
#define NOTE_AIS2  9323
#define NOTE_H2    9878

#define NOTE_C3    10465
#define NOTE_CIS3  11087
#define NOTE_D3    11747
#define NOTE_DIS3  12445
#define NOTE_E3    13185
#define NOTE_F3    13969
#define NOTE_FIS3  14800
#define NOTE_G3    15680
#define NOTE_GIS3  16612
#define NOTE_A3    17600
#define NOTE_AIS3  18647
#define NOTE_H3    19755

#define NOTE_C4    20930

// Hlasitost jako utlum po 3 dB :

#define VOL_10       0
#define VOL_9        1
#define VOL_8        2
#define VOL_7        3
#define VOL_6        4
#define VOL_5        5
#define VOL_4        6
#define VOL_3        7
#define VOL_2        8
#define VOL_1        9

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void PcaInit( void);
// Inicializace modulu

//--- Zvuk --------------------------------------------------------------------

// Asynchronni pipani :

#ifdef PCA_ASYNC_BEEP
   extern byte _pca_duration;       // citac delky

#define PcaBeep( f, v, duration) _pca_duration = (duration) / TIMER0_PERIOD; PcaSound( f, v)
// Asynchronne pipne kmitoctem <f>, hlasitosti <v> v trvani <duration> milisekund

#define PcaSBeep( f, v, duration) PcaSound( f, v); SysDelay( duration); PcaNosound(); SysDelay(10)
// Synchronne pipne kmitoctem <f>, hlasitosti <v> v trvani <duration> milisekund

#define PcaTrigger()   CheckTrigger( _pca_duration, PcaNosound())
/// Tato funkce se zaradi do interruptu casovace 0

#endif

#define PcaSound( f, v)    \
        PcaSoundRun( FXTAL * PCA_X2 * 10 / 4 / (f), v);
// Zahraje ton <f> s hlasitosti <v>
// Kmitocet je v desetinach Hz, <f> 10 je 1Hz

void PcaNosound( void);
// Zastavi zvuk

#include "Com2sw.h"    // COM2 interface
#include "Com3sw.h"    // COM3 interface

//--- pomocne funkce, nepouzivat ----------------------------------------------

void PcaSoundRun( word count, byte attenuator);
// Spusti zvuk s periodou <count> a  utlumem <attenuator>

#endif
