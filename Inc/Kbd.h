//*****************************************************************************
//
//    Kbd.h -  Keyboard services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Kbd_H__
   #define __Kbd_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // kody klaves
#endif

extern byte __xdata__ _kbd_counter;       // citac delky

// Tato funkce se zaradi do interruptu casovace 0 :
#define KbdTrigger()   if( _kbd_counter & 0x3F){   \
                          --_kbd_counter;          \
                       }                           \

void KbdInit(void);
// Inicializace

byte KbdPowerUpKey( void);
// Vrati klavesu, ktera je drzena po zapnuti nebo K_RELEASED

void KbdPowerUpRelease( void);
// Ceka na pusteni klavesy, drzene po zapnuti

byte KbdGet( void);
// Testuje stisknuti klavesy, vraci klavesu nebo
// K_IDLE neni-li stisknuto nic, volat periodicky

//------------ funkce standardniho vstupu :

byte getch( void);
// Ceka na stisknuti klavesy, vrati ji

#endif
