//*****************************************************************************
//
//    Ug3264gm.c51  -  Graficky displej Univision UG-3264GMCAT01 (132x64)
//    Verze 1.0
//
//*****************************************************************************

#ifndef __UG3264GM_H__
   #define __UG3264GM_H__

#include "Hardware.h"     // zakladni datove typy

// Rozmery displeje:
#define DISPLAY_HEIGHT_BYTE     (DISPLAY_HEIGHT / 8)    // Vyska v bajtech (osmice pixelu)

// Kontrast:
#define DISPLAY_MIN_CONTRAST    0x00                    // Minimalni hodnota kontrastu
#define DISPLAY_MAX_CONTRAST    0xFF                    // Maximalni hodnota kontrastu

void DisplayInit(void);
  // Inicializace displeje

void DisplayClear(void);
  // Smazani celeho displeje

void DisplaySetCursor(byte XPixel, byte YByte);
  // Nastavi kurzor ve video RAM

byte DisplayRead(void);
  // Nacte bajt z aktualni pozice ve video RAM

#define DisplayWrite(Data)  Ssd1303WriteData(Data)
  // Zapise bajt do aktualni pozice ve video RAM

void DisplayWrite(byte Data);
  // Zapise bajt do aktualni pozice ve video RAM

void DisplayOn(void);
  // Roznuti displeje

void DisplaySetContrast(byte Contrast);
  // Nastavi kontrast displeje na <Contrast> v mezich DISPLAY_MIN_CONTRAST a DISPLAY_MAX_CONTRAST

void DisplaySetNormalDisplay(void);
  // Nastavi pozitivni zobrazeni

void DisplaySetInverseDisplay(void);
  // Nastavi inverzni zobrazeni

void DisplaySetAreaFast(byte X1, byte Y1, byte X2, byte Y2, byte Value);
  // Nastavi rychle oblast displeje na hodnotu <Value>. Y1 a Y2 je cislo radku, radek je vysoky 8 bodu, X1 a X2 muze byt libovolne.

#define DisplayClearAreaFast(X1, Y1, X2, Y2) DisplaySetAreaFast(X1, Y1, X2, Y2, 0);

void DisplaySetArea(byte X1, byte Y1, byte X2, byte Y2, TYesNo Value);
  // Nastavi oblast displeje na hodnotu <Value>. X1, X2, Y1 a Y2 jsou normalni souradnice.

#define DisplayClearArea(X1, Y1, X2, Y2) DisplaySetArea(X1, Y1, X2, Y2, 0);

void DisplayHorLine(byte X, byte Y, byte Delka, byte Increment);
  // Zobrazi vodorovnou caru na pozici X,Y s delkou Delka, pri Increment > 1 udela teckovanou caru

void DisplayFastVertLine(byte X, byte Y, byte Vyska, byte Value);
  // Zobrazi rychle svislou linku na pozici X,Y s vyskou Vyska, ktera je v nasobcich 8.

void DisplayVertLine(byte X, byte Y, byte Vyska);
  // Zobrazi svislou linku na pozici X,Y s vyskou Vyska, ktera je v bodech

void DisplayPixel(byte X, byte Y);
  // Nakresli bod na pozici X,Y

void DisplayChar(byte X, byte Y, byte Znak);
  // Zobrazi znak <Znak> zvolenym fontem na pozici X,Y (v pixelech)

void DisplaySymbol(byte X, byte Y, byte *Symbol);
  // Zobrazi symbol na pozici X,Y

#endif
