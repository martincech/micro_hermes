//******************************************************************************
//
//   MBCb.h          Modbus callback definitions
//   Version 1.0 (c) VymOs
//
//******************************************************************************

#ifndef __MBCb_H__
   #define __MBCb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Vykonne funkce protokolu vraceji NO v pripade chyby,
// Musi predtim zavolat MBException( <kod chyby>) viz
// MBPdu.h

// Parametry komunikace --------------------------------------------------------

word MBCGetBaud( void);
// Vrati baudovou rychlost

byte MBCGetParity( void);
// Vrati paritu (viz MBCom.h)

word MBCGetTimeout( void);
// Vrati timeout v [ms]

#ifndef MB_FIXED_MODE

byte MBCGetMode( void);
// Vrati mod komunikace (viz MBPacket.h)

#endif // MB_FIXED_MODE

#ifdef COM_TX_SPACE

word MBCGetTxSpace( void);
// Vrati prodlevu mezi Rx a Tx

#endif // COM_TX_SPACE

// Vlastni adresa zarizeni -----------------------------------------------------

byte MBCGetOwnAddress( void);
// Vrati vlastni adresu zarizeni

word MBCGetSlaveId( void);
// Vrati identifikaci zarizeni

// Vykonne funkce --------------------------------------------------------------
#ifdef MB_ENABLE_DISCRETE

TYesNo MBCReadDiscrete( word Address, word Count);
// Precte pole binarnich hodnot o delce <Count> bitu

#endif // MB_ENABLE_DISCRETE
//-----------------------------------------------------------------------------
#ifdef MB_ENABLE_COILS

TYesNo MBCReadCoils( word Address, word Count);
// Precte pole binarnich hodnot o delce <Count> bitu

TYesNo MBCWriteSingleCoil( word Address, word Value);
// Zapise binarni hodnotu

TYesNo MBCWriteCoils( word Address, word Count, byte *Data);
// Zapis pole binarnich hodnot o delce <Count> bitu. <Data> je pole bitu

#endif // MB_ENABLE_COILS
//-----------------------------------------------------------------------------
#ifdef MB_ENABLE_INPUT_REGISTERS

TYesNo MBCReadInputRegisters( word Address, word Count);
// Precte pole 16bit hodnot o poctu <Count>

#endif // MB_ENABLE_INPUT_REGISTERS
//-----------------------------------------------------------------------------
#ifdef MB_ENABLE_REGISTERS

TYesNo MBCReadRegisters( word Address, word Count);
// Precte pole 16bit hodnot o poctu <Count>

TYesNo MBCWriteSingleRegister( word Address, word Value);
// Zapis 16bit hodnoty <Value>

TYesNo MBCWriteRegisters( word Address, word Count, word *Data);
// Zapis pole 16bit hodnot o poctu <Count>. <Data> je pole hodnot

TYesNo MBCMaskWriteRegister( word Address, word AndMask, word OrMask);
// Zapis registru pomoci masky

#endif // MB_ENABLE_REGISTERS
//-----------------------------------------------------------------------------
#ifdef MB_ENABLE_FIFO

word MBCGetFifoLength( void);
// Vrati pocet polozek FIFO

TYesNo MBCReadFifoQueue( word Address, word Count);
// Precte frontu 16bit hodnot v poctu <Count>

#endif // MB_ENABLE_FIFO
//-----------------------------------------------------------------------------

#endif
