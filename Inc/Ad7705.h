//*****************************************************************************
//
//    Ad7705.h  -  AD7705 services
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Ad7705_H__
   #define __Ad7705_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Konstanty pro volbu zesileni - ORuje se do setup registru, jde o bity G2, G1 a G0
#define ADC_GAIN_1         0x00
#define ADC_GAIN_2         0x08
#define ADC_GAIN_4         0x10
#define ADC_GAIN_8         0x18
#define ADC_GAIN_16        0x20
#define ADC_GAIN_32        0x28
#define ADC_GAIN_64        0x30
#define ADC_GAIN_128       0x38
// Vyber unipolarniho/bipolarniho modu - ORuje se do setup registru, jde o bit B/U
#define ADC_UNIPOLAR_MODE  0x04
#define ADC_BIPOLAR_MODE   0x00
// Volba bufferu - ORuje se do setup registru, jde o bit BUF
#define ADC_BUFFER_ON      0x02
#define ADC_BUFFER_OFF     0x00



void AdcInit();
// Inicializuje sbernice a kalibruje prevodnik podle nastavenych parametru

void AdcSetInput(bit Vstup);
// Nastavi prevodnik na zadany vstup

word AdcRead(bit Vstup);
// Adresuje kanal <Vstup>, vrati prectenou hodnotu

#endif

