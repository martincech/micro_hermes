//*****************************************************************************
//
//    Ssd1306.h  -  Display driver SSD1306
//    Version 1.0
//
//*****************************************************************************

#ifndef __SSD1306_H__
   #define __SSD1306_H__

#include "Hardware.h"     // zakladni datove typy

//-----------------------------------------------------------------------------
// Prikazy
//-----------------------------------------------------------------------------

#define SSD1306_SET_DISPLAY_CLOCKDIV                    0xD5
#define SSD1306_SET_LOWER_COLUMN_ADDRESS                0x00
#define SSD1306_SET_HIGHER_COLUMN_ADDRESS               0x10
#define SSD1306_SET_DISPLAY_START_LINE                  0x40
#define SSD1306_SET_CONTRAST_CONTROL_REGISTER           0x81
#define SSD1306_SET_SEGMENT_REMAP_0                     0xA0
#define SSD1306_SET_SEGMENT_REMAP_1                     0xA1
#define SSD1306_SET_NORMAL_DISPLAY                      0xA6
#define SSD1306_SET_INVERSE_DISPLAY                     0xA7
#define SSD1306_SET_MULTIPLEX_RATIO                     0xA8
#define SSD1306_SET_DISPLAY_OFF                         0xAE
#define SSD1306_SET_DISPLAY_ON                          0xAF
#define SSD1306_SET_PAGE_ADDRESS                        0xB0
#define SSD1306_SET_COM_OUTPUT_SCAN_DIRECTION_NORMAL    0xC0
#define SSD1306_SET_COM_OUTPUT_SCAN_DIRECTION_REMAPPED  0xC8
#define SSD1306_SET_DISPLAY_OFFSET                      0xD3
#define SSD1306_SET_DC_CONVERTER                        0x8D
#define SSD1306_SET_DC_CONVERTER_ON                     0x14
#define SSD1306_SET_DC_CONVERTER_OFF                    0x10
#define SSD1306_SET_COM_PINS                            0xDA
#define SSD1306_SET_PRECHARGE                           0xD9
#define SSD1306_SET_VCOMH_DESELECT_LEVEL                0xDB
#define SSD1306_SET_ENTIRE_DISPLAY_ON                   0xA4

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Ssd1306WriteCommand(byte Command);
  // Zapise do displeje prikaz <Command>

byte Ssd1306ReadStatus(void);
  // Nacte status displeje

void Ssd1306WriteData(byte Data);
  // Zapise do displeje data <Data>

byte Ssd1306ReadData(void);
  // Nacte z displeje data

void Ssd1306Init(void);
  // Inicializace radice


#endif

