//*****************************************************************************
//
//    Ds17287.h  -  Real time clock DS17287 services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ds17287_H__
   #define __Ds17287_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Vyctovy typ pro dny v tydnu :

typedef enum {
   RTC_PO,
   RTC_UT,
   RTC_ST,
   RTC_CT,
   RTC_PA,
   RTC_SO,
   RTC_NE
} TRtcDow;

//-----------------------------------------------------------------------------
// Inicializace a cteni/zapis z RAM
//-----------------------------------------------------------------------------

TYesNo RtcInit( void);
// Inicializuje sbernici a RTC

//TYesNo RtcWrite( byte address, byte value);
// Zapise <value> na <address> do RAM

//byte RtcRead( byte address);
// Precte obsah RAM z <address> a vrati jej

//-----------------------------------------------------------------------------
// Cteni hodin
//-----------------------------------------------------------------------------

byte RtcSec( void);
// Vrati sekundy hodin v BCD

byte RtcMin( void);
// Vrati minuty hodin v BCD

byte RtcHour( void);
// Vrati hodiny hodin v BCD

//-----------------------------------------------------------------------------
// Cteni data
//-----------------------------------------------------------------------------

byte RtcDay( void);
// Vrati den v mesici hodin v BCD

byte RtcMonth( void);
// Vrati mesic hodin v BCD

word RtcYear( void);
// Vrati rok hodin v BCD

byte RtcWday( void);
// Vrati den v tydnu hodin ve forme vyctu

//-----------------------------------------------------------------------------
// Zapis hodin
//-----------------------------------------------------------------------------

void RtcSetSec( byte bcd);
// Nastavi sekundy hodin, <bcd> je hodnota v BCD

void RtcSetMin( byte bcd);
// Nastavi minuty hodin, <bcd> je hodnota v BCD

void RtcSetHour( byte bcd);
// Nastavi sekundy hodin, <bcd> je hodnota v BCD

//-----------------------------------------------------------------------------
// Zapis data
//-----------------------------------------------------------------------------

void RtcSetDay( byte bcd);
// Nastavi den v mesici hodin, <bcd> je hodnota v BCD

void RtcSetMonth( byte bcd);
// Nastavi mesic hodin, <bcd> je hodnota v BCD

void RtcSetYear( word bcd);
// Nastavi rok hodin, <bcd> je hodnota v BCD

void RtcSetWday( byte dow);
// Nastavi den v tydnu hodin, <dow> je den v tydnu

//-----------------------------------------------------------------------------
// Nastaveni alarmu (budiku) na cas
//-----------------------------------------------------------------------------

void RtcSetAlarm(byte HourBcd, byte MinuteBcd);
// Nastavi alarm na cas Hour:Minute, cas se zadava v bcd

bit RtcCheckAlarm();
// Zkontroluje, zda nenastal alarm (tj. zda je nahozeny flag alarmu)

void RtcAlarmOff();
// Vypne uplne alarm

//-----------------------------------------------------------------------------
// Obsluha NVRAM
//-----------------------------------------------------------------------------

void RtcRamWrite( word Address, byte Value);
// Zapise do NVRAM

byte RtcRamRead( word Address);
// Precte z NVRAM

#endif
