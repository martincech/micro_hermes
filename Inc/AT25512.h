//*****************************************************************************
//
//    AT25512.c51  AT25512/AT25256 EEPROM via HW SPI
//    Version 1.0
//
//*****************************************************************************

#ifndef __AT25512_H__
   #define __AT25512_H__

#include "Hardware.h"
#include "..\inc\Spi.h"

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void EepInit();
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

byte EepReadByte( word address);
// Precte byte z EEPROM <address>

TYesNo EepWriteByte( word address, byte value);
// Zapise byte <value> na <address> v EEPROM

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word address);
// Zahaji blokove cteni z EEPROM <address>

#define EepBlockReadData() SpiReadByte()
// Cteni bytu bloku

#define EepBlockReadStop() SpiRelease()
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( word address);
// Zahaji zapis stranky od <address> v EEPROM
// Vraci NO neni-li zapis mozny

#define EepPageWriteData( value) SpiWriteByte( value)
// Zapis bytu do stranky

#define EepPageWritePerform() SpiRelease()
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Ulozeni dat -----------------------------

TYesNo EepSaveData(word Address, byte __xdata__ *Data, byte Size);
  // Ulozi na adresu <Address> data v <Data> o velikosti <Size> bajtu

void EepReadData(word Address, byte __xdata__ *Data, byte Size);
  // Nacte data od adresy <Address> o velikosti <Size> bajtu do promenne <Data>

TYesNo EepFill(word StartAddress, word EndAddress, byte Value);
  // Vyplni oblast <StartAddress> az <EndAddress> vcetne hodnotou <Value>


#endif // __AT25512_H__
