//*****************************************************************************
//
//    Hd44780.h - HD44780 LCD display services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Hd44780_H__
   #define __Hd44780_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Interni promenne, nemodifikovat ! :

extern byte _row;
extern byte _col;

// mezera bez prepsani podkladu :

#define CH_NULL           0


void DisplayInit( void);
// inicializuje display

void DisplayCursor( bool on);
// je-li <on>=NO zhasne kurzor, jinak rozsviti

void DisplayClear( void);
// smaze display

#define DisplayGotoRC( r, c)  _row = (r); _col = (c)
// presune kurzor na zadanou textovou pozici <r> radek <c> sloupec

void DisplayClrEol( void);
// smaze az do konce radku, nemeni pozici kurzoru

char putchar( char c);
// standardni zapis znaku

#endif

