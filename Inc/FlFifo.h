//*****************************************************************************
//
//    FlFifo.h     - Flash FIFO implementation
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __FlFifo_H__
   #define __FlFifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // zakladni datove typy
#endif

// POZOR, zadna funkce nedava FlashFlush

typedef dword TFifoIndex;           // index polozky - u Bat2 online mereni je cca 700tis polozek
typedef word  TFifoItemSize;        // velikost polozky
typedef byte  TFifoMarker;          // znacka
typedef dword TFifoAddress;         // fyzicka adresa

#define FIFO_INVALID_INDEX   0xFFFFFFFF // neplatny index do FIFO

typedef struct {
   TFifoAddress  Base;              // bazova adresa
   TFifoIndex    Size;              // pocet polozek
   TFifoItemSize ItemSize;          // velikost polozky
   TFifoMarker   MarkerEmpty;       // znacka nenaplneneho FIFO
   TFifoMarker   MarkerFull;        // znacka naplneneho FIFO

   TFifoIndex    MarkerIndex;       // index znacky
   TFifoMarker   Marker;            // aktualni znacka
} TFifo;


void FifoInit( TFifo __xdata__ *Fifo);
// Inicializace

void FifoReset( TFifo __xdata__ *Fifo);
// Smaze obsah (vcetne flush)

#define FifoWrite( Fifo, Item) FifoNewFragment( Fifo, Item, (Fifo)->ItemSize)
// Zapis nove polozky

#define FifoUpdate( Fifo, Index, Item) FifoUpdateFragment( Fifo, Index, Item, (Fifo)->ItemSize)
// Aktualizace polozky na pozici <Index>

TYesNo FifoNewFragment( TFifo __xdata__ *Fifo, void __xdata__ *Item, TFifoItemSize Size);
// Zapis fragmentu nove polozky

TYesNo FifoUpdateFragment( TFifo __xdata__ *Fifo, TFifoIndex Index, void __xdata__ *Item,
                           TFifoItemSize Offset, TFifoItemSize Size);
// Aktualizace fragmentu polozky na pozici <Index>

#define FifoRead( Fifo, Index, Item) FifoReadFragment( Fifo, Index, Item, 0, (Fifo)->ItemSize)
// Cteni polozky z pozice

void FifoReadFragment( TFifo __xdata__ *Fifo, TFifoIndex Index, void __xdata__ *Item,
                       TFifoItemSize Offset, TFifoItemSize Size);
// Cteni polozky z pozice, nacte fragment polozky s posunem
// <Offset> ve velikosti <Size>

TFifoAddress FifoItemAddress( TFifo __xdata__ *Fifo, TFifoIndex Index, TFifoItemSize Offset);
// Fyzicka adresa (fragmentu) polozky

TFifoIndex FifoFirst( TFifo __xdata__ *Fifo);
// Vrati index prvni zapsane polozky

TFifoIndex FifoLast( TFifo __xdata__ *Fifo);
// Vrati index posledni zapsane polozky

TFifoIndex FifoPrevious( TFifo __xdata__ *Fifo, TFifoIndex Index);
// Vrati index predchozi polozky

TFifoIndex FifoNext( TFifo __xdata__ *Fifo, TFifoIndex Index);
// Vrati index nasledujici polozky

#define FifoFull(Fifo) ((Fifo)->Marker == (Fifo)->MarkerFull)
// Pokud je fifo plne a prepisuje se, vrati YES

#define FifoEmpty(Fifo) ((Fifo)->Marker == (Fifo)->MarkerEmpty && (Fifo)->MarkerIndex == 0)
// Pokud je fifo prazne (neobsahuje zadny zaznam), vrati YES


#endif
