//*****************************************************************************
//
//    Ads1241f.h  -  A/D convertor ADS1241 single channel with filtering
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads1241f_H__
   #define __Ads1241f_H__

#ifndef __Filter_H__
   #include "Filter.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

#define AdcIsReady()         (!AdcDRDY)
// Prevodnik ma pripravena data

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

void AdcSelectInput( byte Channel);
// Prepnuti multiplexoru
// parametr <Channel> je cislo kanalu 0.. ADC_COUNT-1

dword AdcReadValue( void);
// Precteni namerene hodnoty

//-----------------------------------------------------------------------------

void AdcStart( byte Channel);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

TRawWeight AdcRawRead( void);
// Cteni okamzite hodnoty prevodu


// POZOR, nasledujici dve funkce volat pri zakazanem preruseni

#define AdcDataReady()   GetFilterReady()
// Test na nove ustalenou hodnotu

TRawWeight AdcRead( void);
// Cteni ustalene hodnoty (volat jen po AdcDataReady = true)
// Vrati rozdil ustalenych hodnot

#endif
