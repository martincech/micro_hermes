//*****************************************************************************
//
//    S6b1713.c  -  Display driver Samsung S6B1713
//    Version 1.0
//
//*****************************************************************************

#ifndef __S6B1713_H__
   #define __S6B1713_H__

#include "Hardware.h"     // zakladni datove typy

//-----------------------------------------------------------------------------
// Prikazy
//-----------------------------------------------------------------------------

#define S6B1713_SET_DISPLAY_OFF                         0xAE
#define S6B1713_SET_DISPLAY_ON                          0xAF
#define S6B1713_SET_DISPLAY_INITIAL_LINE                0x40
#define S6B1713_SET_REFERENCE_VOLTAGE_MODE              0x81
#define S6B1713_SET_REFERENCE_VOLTAGE_REGISTER          0x00
#define S6B1713_SET_PAGE_ADDRESS                        0xB0
#define S6B1713_SET_LOWER_COLUMN_ADDRESS                0x00
#define S6B1713_SET_HIGHER_COLUMN_ADDRESS               0x10
#define S6B1713_SET_ADC_SELECT_NORMAL                   0xA0
#define S6B1713_SET_ADC_SELECT_REVERSE                  0xA1
#define S6B1713_SET_NORMAL_DISPLAY                      0xA6
#define S6B1713_SET_INVERSE_DISPLAY                     0xA7
#define S6B1713_SET_ENTIRE_DISPLAY_ON                   0xA5
#define S6B1713_SET_ENTIRE_DISPLAY_OFF                  0xA4
#define S6B1713_SET_LCD_BIAS_SELECT                     0xA2
#define S6B1713_SET_MODIFY_READ                         0xE0
#define S6B1713_SET_RESET_MODIFY_READ                   0xEE
#define S6B1713_SET_RESET                               0xE2
#define S6B1713_SET_SHL_SELECT_NORMAL                   0xC0
#define S6B1713_SET_SHL_SELECT_REVERSE                  0xC8
#define S6B1713_SET_POWER_CONTROL                       0x28
#define S6B1713_SET_REGULATOR_RESISTOR_SELECT           0x20
#define S6B1713_SET_STATIC_INDICATOR_MODE               0xAC
#define S6B1713_SET_STATIC_INDICATOR_REGISTER           0x00

// Hodnoty pro S6B1713_SET_POWER_CONTROL:
#define S6B1713_POWER_CONVERTER_ON                      0x04
#define S6B1713_POWER_REGULATOR_ON                      0x02
#define S6B1713_POWER_FOLLOWER_ON                       0x01

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void S6b1713WriteCommand(byte Command);
  // Zapise do displeje prikaz <Command>

byte S6b1713ReadStatus(void);
  // Nacte status displeje

void S6b1713WriteData(byte Data);
  // Zapise do displeje data <Data>

byte S6b1713ReadData(void);
  // Nacte z displeje data

void S6b1713Init(void);
  // Inicializace radice


#endif

