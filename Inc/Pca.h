//*****************************************************************************
//
//    Pca.h  -  PCA sound generation only
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Pca_H__
   #define __Pca_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Trocha hudby :

#define NOTE_C1    2616
#define NOTE_CIS1  2772
#define NOTE_D1    2937
#define NOTE_DIS1  3111
#define NOTE_E1    3296
#define NOTE_F1    3492
#define NOTE_FIS1  3700
#define NOTE_G1    3920
#define NOTE_GIS1  4153
#define NOTE_A1    4400
#define NOTE_AIS1  4661
#define NOTE_H1    4939

#define NOTE_C2    5233
#define NOTE_CIS2  5544
#define NOTE_D2    5873
#define NOTE_DIS2  6223
#define NOTE_E2    6593
#define NOTE_F2    6985
#define NOTE_FIS2  7400
#define NOTE_G2    7840
#define NOTE_GIS2  8306
#define NOTE_A2    8800
#define NOTE_AIS2  9323
#define NOTE_H2    9878

#define NOTE_C3    10465
#define NOTE_CIS3  11087
#define NOTE_D3    11747
#define NOTE_DIS3  12445
#define NOTE_E3    13185
#define NOTE_F3    13969
#define NOTE_FIS3  14800
#define NOTE_G3    15680
#define NOTE_GIS3  16612
#define NOTE_A3    17600
#define NOTE_AIS3  18647
#define NOTE_H3    19755

#define NOTE_C4    20930

// Hlasitost jako sirka impulsu :

#define VOL_10   50000
#define VOL_9     7000
#define VOL_8     4500
#define VOL_7     3000
#define VOL_6     2000
#define VOL_5     1500
#define VOL_4     1000
#define VOL_3      900
#define VOL_2      800
#define VOL_1      700

// Asynchronni pipani :

#ifdef PCA_ASYNC_BEEP
   extern byte _pca_counter;       // citac delky

#define PcaBeep( f, w, duration) _pca_counter = (duration) / TIMER0_PERIOD; PcaSound( f, w)
// Asynchronne pipne kmitoctem <f>, sirkou <w> v trvani <duration> milisekund

#define PcaSBeep( f, w, duration) PcaSound( f, w);delay( (duration) / TIMER0_PERIOD);PcaStop();delay(10)
// Synchronne pipne kmitoctem <f>, sirkou <w> v trvani <duration> milisekund

#define PcaTrigger()   CheckTrigger( _pca_counter, PcaStop())
/// Tato funkce se zaradi do interruptu casovace 0

#endif

//-----------------------------------------------------------------------------

#define PcaSound( f, w)    \
        PcaRun( FXTAL * PCA_X2 * 10 / 4 / (f), (FXTAL * PCA_X2 * 10 / 4 / (f)) * (w) / 100000L);
// Zahraje ton <f> s pomerem impuls mezera <w>
// Kmitocet je v desetinach Hz, <f> 10 je 1Hz
// <w> je pomer impuls/mezera 50000=50%

void PcaRun( word count, word pwm);
// Spusti casovac PCA s periodou <count>,
// Komparator preklapi po nacitani <pwm> pulsu

void PcaStop( void);
// Zastavi casovac PCA

#endif
