//*****************************************************************************
//
//    Ds18x20.h  -  1-Wire temperature sensor services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Ds18x20_H__
   #define __Ds18x20_H__

#ifndef __Mwi_H__
   #include "Mwi.h"
#endif

#include <math.h>                       // fce abs()

// Pozor, vzhledem k interni reprezentaci nelze pouzivat
// operator ==, povoleny jsou jen >, >=, <, <=
// analogicky k operacim s datovym typem float.
// Mnozina hodnot je nespojita, napr. nelze presne
// vytvorit v interni reprezentaci teplotu 0.2 C

// Makra na nastavovani hodnot z C do interni reprezentace :
// Zaporne teploty se nastavuji napr. jako t=-tempMk01C(1,5), znamena -1.5C
#define TempMk1C( c)           ((int)(c)  * 256)                         // c - cele stupne
#define TempMk01C( c, c01)     (((int)(c) * 256) | ((c01) * 256) / 10)   // c - cele stupne, c01  = 0..9 desetiny stupne
#define TempMk001C( c, c001)   (((int)(c) * 256) | ((c001) * 256) / 100) // c - cele stupne, c001 = 0..99 setiny stupne

// Makra pro cteni hodnot z interni reprezentace : - vraci vzdy absolutni hodnotu, znamenko musim hlidat zvlast - jsou to makra pouze na zobrazovani teploty, takze to jde v pohode
// Musim pouzit vzdy fci abs(), aby se odstranil pripadny dvojkovy doplnek
#define TempGet1C( t)          abs((int)((int)(t)/(int)256))      // vrati cele stupne
#define TempGet01C( t)         ((((abs((int)t)) & 0xFF) * 10)  >> 8)        // vrati desetiny stupne 0..9
#define TempGet001C( t)        ((((abs((int)t)) & 0xFF) * 100) >> 8)        // vrati setiny stupne   0..99

// Mezni hodnoty :

#define TEMP_MAX_C    84                                           // maximum merene senzorem
#define TEMP_INITIAL  85                                           // neinicializovany senzor
#define TEMP_MIN_C   -40                                           // minimum merene senzorem
#define TEMP_MAX      TempMk1C( TEMP_MAX_C)                        // maximalni interni reprezentace
#define TEMP_MIN      TempMk1C( TEMP_MIN_C)                        // minimalni interni reprezentace
#define TEMP_INVALID  TempMk1C( -100)                              // vraci TempRead pri chybe



#ifdef TEMP_DS18S20
   #define TEMP_CHIP_FAMILY 0x10          // Kod cipu (viz mwi_identification_t - family)
   #define TEMP_MASK        0xFF80        // maska platnych mist
   #define TEMP_SHIFT       7             // posun vlevo
#endif
#ifdef TEMP_DS18B20
   #define TEMP_CHIP_FAMILY 0x28          // Kod cipu (viz mwi_identification_t - family)
   #define TEMP_MASK        0xFFF0        // maska platnych mist
   #define TEMP_SHIFT       4             // posun vlevo
#endif


// Inicializace templomeru :

#ifdef MwiDATA
   // sdilena sbernice
   #define TempInit()   MwiAttach();MwiRelease()  // definovani stavu
#else
   // individualni pripojeni
   #define TempInit()   MwiAttach()               // trvala inicializace
#endif

void TempStartConversion( void);
// Vysle prikaz zacatku konverze

int TempRead( byte channel);
// Vrati prectenou teplotu, v hornim bytu jsou stupne C,
// v dolnim pokracuji desetiny (MSB = 0.5, 0.25, 0.125...LSB)

void TempPowerOff();
// Vypne napajeni cidlum (opetovne zapnuti pomoci TempStartConversion())


#endif

