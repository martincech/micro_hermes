//******************************************************************************
//
//   MBPacket.h      Modbus serial communication - RTU/ASCII packets
//   Version 1.0 (c) VymOs
//
//******************************************************************************

#ifndef __MBPacket_H__
   #define __MBPacket_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MBDef_H__
   #include "MBDef.h"
#endif

//------------------------------------------------------------------------------

void MBSetMode( word Baud, byte Parity, word Timeout, byte Mode);
// Nastavi rezim paketove urovne

#ifdef COM_TX_SPACE
   #ifndef __MBCom_H__
      #include "MBCom.h"
   #endif

#define MBSetTxSpace( Timeout) ComSetTxSpace( Timeout)
// Nastavi mezeru mezi Rx a Tx

#endif // COM_TX_SPACE

//------------------------------------------------------------------------------
//   Rx
//------------------------------------------------------------------------------

TYesNo MBReceive( void **Pdu, word *Size);
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>

byte MBGetAddress( void);
// Vrati adresu paketu. POZOR, volat az po MBRReceive

//------------------------------------------------------------------------------
//   Tx
//------------------------------------------------------------------------------

void MBWriteAddress( byte Address);
// Zapise adresu paketu

void MBWriteByte( byte Data);
// Zapise 8bitove slovo do paketu

void MBWriteWord( word Data);
// Zapise 16bitove slovo do paketu

void MBWriteCrc( void);
// Spocita a zapise zabezpeceni

#endif
