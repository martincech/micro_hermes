//*****************************************************************************
//
//    Alu.h        Arithmetic utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Alu_H__
   #define __Alu_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif
#include <string.h>

//-----------------------------------------------------------------------------
// nulovani
//-----------------------------------------------------------------------------

#define qzero( dst)       memset( (dst), 0, sizeof( qword))
#define qmovq( dst, src)  memcpy( (dst), (src), sizeof( qword))
#define qmovd( dst, src)  (dst)->dw[ 0] = 0; (dst)->dw[ 1] = *(src)
#define qmovw( dst, src)  (dst)->dw[ 0] = 0; (dst)->w[ 2] = 0; (dst)->w[ 3]= *(src)

//-----------------------------------------------------------------------------
// scitani
//-----------------------------------------------------------------------------

void qaddq( qword data *dst, qword data *src);

void qaddd( qword data *dst, dword data *src);

//-----------------------------------------------------------------------------
// odcitani
//-----------------------------------------------------------------------------

void qsubq(  qword data *dst, qword data *src);

void qsubd(  qword data *dst, dword data *src);

TYesNo qminus( qword data *dst);  // test na zaporny vysledek

//-----------------------------------------------------------------------------
// nasobeni
//-----------------------------------------------------------------------------

void qmuldd( qword data *dst, dword data *src1, dword data *src2);
void qsqrq( qword data *dst, qword data *src);

//-----------------------------------------------------------------------------
// deleni
//-----------------------------------------------------------------------------

void ddivqq( dword data *dst, qword data *numerator, qword data *denominator);

#endif
