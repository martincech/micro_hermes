Pouziti modulu hodin
--------------------

Modul hodin (dale RTC) sestava z techto zdrojovych textu :

Pcf8583.c     - vykonne funkce RTC
Pcf8583.h     - uzivatelske funkce (API) pro obsluhu RTC
Pcf8583.hw    - soubor popisujici HW pripojeni sbernice,
                bude pouzit jako sekce v souboru Hardware.h projektu

Dalsi zdrojove texty slouzi pro testovani API RTC :

Hardware.h  - HW pripojeni RTC
Main.c      - pouziti vsech funkci API RTC
Pcf8583.prj - projektovy soubor pro MEW
Makefile    - soubor na sestaveni testu

Predpokladam nasledujici nasazeni :

1. Uzivatel upravi Hardware.h podle aktualni
   HW konfigurace. Modul Hardware.h je v dane aplikaci
   spolecny pro vsechny druhy zarizeni. Ze vzoroveho modulu
   Pcf8583.hw se do nej zaradi sekce tykajici se RTC

3. Dale k RTC pristupuje pomoci funkci API (Pcf8583.h)
4. Uzivatel by nemel modifikovat soubory Pcf8583.c, h, Iic.c, h


Komentar k implementaci :
-------------------------

Hodiny ukladaji cas, datum a den v tydnu. Pokud se rozhodneme
merit cas na setiny sekundy, pouzijeme konstantu RTC_USE_HSEC.
Potrebujeme-li datum pouzijeme konstantu RTC_USE_DATE (neni-li
definovana, prekladaji se jen funkce pro cas).
Konstantou RTC_USE_WDAY povolime preklad funkci na nastaveni/cteni
dne v tydnu. Konstanta RTC_RANGE_CHECK povoluje preklad hlidani
mezi zadavanych hodnot. Bez teto konstanty za spravne meze
zodpovida uzivatel API.


Popis API hodin RTC
------------------------

PCF 8583 obsahuje vnitrni pamet 256 bytu, ktera se castecne
prekryva s internimi registry hodin. Zbytek pameti je volne dostupny
uzivateli. Adresy pro volne pouziti jsou :

   RTC_RAM_FIRST..RTC_RAM_LAST vcetne.

Funkce pro pristup k pameti se prekladaji vzdy, protoze ostatni
funkce je interne pouzivaji.
   Pro potreby evidence dne v tydnu modul obsahuje vyctovy typ
TRtcDow s definici dnu v tydnu (pro funkce RtcWday/RtcSetWday).
Protoze neni z dokumentace zrejme, jak PCF 8583 pracuje pri
cteni casu (zmineno je latchovani celeho casu po precteni
prvniho udaje, aby nevznikaly chyby pri prenosu do dalsiho
radu mezi dvema ctecimi cykly), bude vhodne pri cteni casu
precist hodnoty sekvencne najednou. Pri zapisu casu/data
je zapisovana hodnota okamzite uplatnena bez latchovani.


TYesNo RtcInit( void)
----------------------

Inicializuje sbernici I2C a nastavi mod prace RTC.
Cas zapsany v RTC neovlivnuje.


TYesNo RtcWrite( byte address, byte value)
------------------------------------------

Zapise byte <value> na adresu <address> do RAM


byte RtcRead( byte address)
---------------------------

Precte obsah RAM z adresy <address> a vrati jej
jako funkcni hodnotu


byte RtcHsec( void)
-------------------
Vrati aktualni cas - setiny sekund v kodu BCD


byte RtcSec( void)
------------------

Vrati aktualni cas - sekundy v kodu BCD


byte RtcMin( void)
------------------

Vrati aktualni cas - minuty v kodu BCD

byte RtcHour( void)
-------------------

Vrati aktualni cas - hodiny v kodu BCD

byte RtcDay( void)
------------------

Vrati aktualni datum - den v mesici v kodu BCD

byte RtcMonth( void)
--------------------

Vrati aktualni datum - cislo mesice v kodu BCD

word RtcYear( void)
-------------------

Vrati aktualni datum - rok v kodu BCD.
Cislo roku je ctyrmistne.


byte RtcWday( void)
-------------------

Vrati aktualni den v tydnu, vyctovym typem TRtcDow

void RtcSetHsec( byte bcd)
--------------------------

Nastavi aktualni cas - setiny sekund. Hodnota <bcd> je v BCD kodovani

void RtcSetSec( byte bcd)
-------------------------

Nastavi aktualni cas - sekundy. Hodnota <bcd> je v BCD kodovani

void RtcSetMin( byte bcd)
-------------------------

Nastavi aktualni cas - minuty. Hodnota <bcd> je v BCD kodovani

void RtcSetHour( byte bcd)
--------------------------

Nastavi aktualni cas - hodiny. Hodnota <bcd> je v BCD kodovani


void RtcSetDay( byte bcd)
-------------------------

Nastavi aktualni datum - den v mesici. Hodnota <bcd> je v BCD kodovani

void RtcSetMonth( byte bcd)
---------------------------

Nastavi aktualni datum - cislo mesice. Hodnota <bcd> je v BCD kodovani

void RtcSetYear( word bcd)
--------------------------

Nastavi aktualni datum - rok. Hodnota <bcd> je ctyrmistna v BCD kodovani

void RtcSetWday( byte dow)
--------------------------

Nastavi aktualni den v tydnu. Hodnota <dow> je z vyctu TRtcDow

