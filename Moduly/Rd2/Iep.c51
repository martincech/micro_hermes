//*****************************************************************************
//
//    Iep.c51  -  89C51 RD2 internal EEPROM services
//    Version 1.2, (c) Vymos
//
//*****************************************************************************


/*
Verze 1.1:
  - 6.9.2003: Ve vsech funkcich, ktere nejak volaji MOVX musi byt zakazane preruseni a to po celou dobu prace s EEPROM,
              tj. zakazat na zacatku fce a povolit az na konci. Jinak to blbne ve spojeni s PCA (Capture/Compare).
Verze 1.2:
  - 7.3.2004:  Doplneno AT89C51ED2
  - 21.4.2004: Upraveno na optimisticke testovani AccuOk
 */

#include "..\inc\Iep.h"


#ifdef __T89C51RD2__

//-----------------------------------------------------------------------------
// Cteni jednoho bytu
//-----------------------------------------------------------------------------

byte IepRead( word address)
// Precte a vrati byte z interni EEPROM na adrese <address>
{
byte value;

   IepDisableInts();                // zakaz preruseni
   EECON |= EECON_EEE;
   value = XBYTE[ address];
   EECON &= ~EECON_EEE;
   IepEnableInts();                 // povoleni preruseni
   return( value);
} // IepRead

//-----------------------------------------------------------------------------
// Zahajeni zapisu
//-----------------------------------------------------------------------------

TYesNo IepPageWriteStart( void)
// Zahajuje zapis do stranky, vraci NO nelze-li zapisovat
{
   if( !IepAccuOk()){
      return( NO);
   }
   return( YES);
} // IepPageWriteStart

//-----------------------------------------------------------------------------
// Zapis do latche
//-----------------------------------------------------------------------------

void IepPageWriteData( word address, byte value)
// Zapis bytu do stranky
{
   IepDisableInts();                // zakaz preruseni
   EECON |= EECON_EEE;
   XBYTE[ address] = value;
   EECON &= ~EECON_EEE;
   IepEnableInts();                 // povoleni preruseni
} // IepPageWriteData

//-----------------------------------------------------------------------------
// Provedeni zapisu
//-----------------------------------------------------------------------------

void IepPageWritePerform()
// Zahaji fyzicky zapis stranky
{
   IepDisableInts();                // zakaz preruseni
   EETIM = IEP_EETIM;               // nastav casovani zapisu
   // Je treba zachovat tuto sekvenci, jinak dela potize :
   EECON = EECON_EEPL1 | EECON_EEE; // prvni kodove slovo + select
   EECON = EECON_EEPL2 | EECON_EEE; // druhe kodove slovo + select
   while( EECON & EECON_EEBUSY);    // cekame na dokonceni zapisu, jinak nefunguje !
   EECON &= ~EECON_EEE;             // deselect
   IepEnableInts();                 // povoleni preruseni
} // IepPageWritePerform()

#ifdef IEP_WRITE_BYTE
//-----------------------------------------------------------------------------
// Zapis jednoho bytu
//-----------------------------------------------------------------------------

TYesNo IepWriteByte( word address, byte value)
// Zapise byte <value> do interni EEPROM na adresu <address>.
// Vraci NO neni-li zapis mozny
{
   if( !IepPageWriteStart()){
      return( NO);
   }
   IepPageWriteData( address, value);
   IepPageWritePerform();
   return( YES);
} // IepWriteByte

#endif

#else
   // AT89C51ED2

//-----------------------------------------------------------------------------
// Cteni jednoho bytu
//-----------------------------------------------------------------------------

byte IepRead( word address)
// Precte a vrati byte z interni EEPROM na adrese <address>
{
byte value;

   while( EECON & EECON_EEBUSY);    // cekame na dokonceni zapisu
   IepDisableInts();                // zakaz preruseni
   EECON |= EECON_EEE;
   value = XBYTE[ address];
   EECON &= ~EECON_EEE;
   IepEnableInts();                 // povoleni preruseni
   return( value);
} // IepRead

//-----------------------------------------------------------------------------
// Zahajeni zapisu
//-----------------------------------------------------------------------------

TYesNo IepPageWriteStart( void)
// Zahajuje zapis do stranky, vraci NO nelze-li zapisovat
{
   if( !IepAccuOk()){
      return( NO);
   }
   return( YES);
} // IepPageWriteStart

//-----------------------------------------------------------------------------
// Surovy zapis jednoho bytu
//-----------------------------------------------------------------------------

void IepRawWriteByte( word address, byte value)
// Zapise byte <value> do interni EEPROM na adresu <address>.
{
   while( EECON & EECON_EEBUSY);    // cekame na dokonceni zapisu
   IepDisableInts();                // zakaz preruseni
   EECON          = EECON_EEE;      // EEPROM data mapping via EEE bit
   XBYTE[address] = value;          // MOVX @DPTR, A
   EECON          = 0x00;           // disable EEPROM mapping
   IepEnableInts();                 // povoleni preruseni
} // IepWriteByte

#ifdef IEP_WRITE_BYTE
//-----------------------------------------------------------------------------
// Zapis jednoho bytu
//-----------------------------------------------------------------------------

TYesNo IepWriteByte( word address, byte value)
// Zapise byte <value> do interni EEPROM na adresu <address>.
// Vraci NO neni-li zapis mozny
{
   if (!IepAccuOk()){
       return( NO);
   }
   IepRawWriteByte( address, value);
   return( YES);
} // IepWriteByte
#endif // IEP_WRITE_BYTE


#endif // AT89C51ED2
