//*****************************************************************************
//
//    Mc33298.c   -  MC33298 power digital output
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "..\inc\Mc33298.h"
#include "..\inc\System.h"    // SysDelay


// POZOR : vstup  SI = H je vypnuto, SI = L je zapnuto
//         vystup SO = H je vypnuto, SO = L je zapnuto

// Lokalni funkce :

static byte RawWrite( byte Value);
// Zapis vystupu/cteni vstupu

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void DigitalOutInit( void)
// Inicializace modulu
{
   DoutCS   = 1;
   DoutSI   = 0;
   DoutSO   = 1;
   DoutSCLK = 0;
   DigitalOutWrite( 0);
} // DoutInit

//-----------------------------------------------------------------------------
// Zapis
//-----------------------------------------------------------------------------

byte DigitalOutWrite( byte Value)
// Zapis vystupu, vraci NO pri chybe
{
byte RetValue;

   DoutSCLK = 0;
   DoutSI   = 0;
   DoutSO   = 1;                       // priprava na cteni
   DoutCS   = 0;                       // select
   RetValue = RawWrite( Value);        // zapis a precteni
   DoutCS   = 1;                       // deselect
   #ifndef DOUT_READBACK
      return( ~RetValue);              // H je vypnuto
   #else
      SysDelay(1);                     // cekej 1ms na ustaleni vystupu
      DoutCS = 0;                      // select
      RetValue = RawWrite( Value);     // zapis a precteni
      DoutCS = 1;                      // deselect
      return( ~RetValue);              // H je vypnuto
   #endif
} // DoutWrite

//-----------------------------------------------------------------------------
// Surovy zapis/cteni
//-----------------------------------------------------------------------------

static byte RawWrite( byte Value)
// Zapis vystupu/cteni vstupu
{
byte RetValue;
byte i;

   RetValue  = 0;
   for( i = 0; i < 8; i++){
      DoutSCLK = 1;                    // aktivace vystupu
      RetValue <<= 1;
      DoutSI = !(Value & 0x80);        // zapisovana hodnota
      if( DoutSO){
         RetValue |= 0x01;
      }
      DoutSCLK = 0;                    // strobovani vstupu
      Value  <<= 1;
   }
   return( RetValue);                  // prectena hodnota
} // DoutWrite

