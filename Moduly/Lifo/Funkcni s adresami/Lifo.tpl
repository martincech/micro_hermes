//*****************************************************************************
//
//    Lifo.tpl - LIFO template module
//    Version 1.0, Petr
//
//*****************************************************************************

// LIFO znamena pamet, do ktere lze zapisovat. Pokud je LIFO cela zaplnena, nelze jiz pridavat dalsi zaznamy.
// Z LIFO lze vymazat posledni ulozeny zaznam, pripadne smazat celou LIFO naraz.

// Data jsou ukladana po zaznamech TLifoData.
// Za poslednim zaznamem je zaznam - marker, ktery ma na prvnim bytu smluveny znak LIFO_MARKER.
// LIFO zacina na adrese LIFO_START_ADDRESS, prvni volna adresa za LIFO je LIFO_NEXT_ADDRESS

#define XLIFO_INVALID_ADDRESS 0xFFFF                                 // neplatna adresa do LIFO
#define XLIFO_NEXT_ADDRESS    (XLIFO_CAPACITY * sizeof( TXLifoData)) // prvni volna adresa za LIFO

// Komunikacni struktura pretypovana na pole :

#define XLifoArray     ((byte __xdata__ *)&XLifoData)    // umozni indexovat po bytu

#ifdef XLIFO_FAST
  static word __xdata__ marker_address;                      // adresa markeru
#endif

// Lokalni funkce :

static TYesNo WriteData(word addr);
// Zapise komunikacni strukturu krome prvniho bytu

#ifdef XLIFO_VERIFY
  static TYesNo VerifyData(word addr);
  // Porovna data zapsana v EEPROM s komunikacni strukturou (vynecha prvni byte)
#endif

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void XLifoInit() {
// Inicializuje modul
  XInit();
#ifdef XLIFO_FAST
  marker_address = XLifoFindMarker();    // nalezeni markeru
#endif
} // XLifoInit

//-----------------------------------------------------------------------------
// Zapis
//-----------------------------------------------------------------------------

TYesNo XLifoWrite() {
// Zapise globalni komunikacni strukturu do LIFO
byte x;
word addr;

#ifdef XLIFO_FAST
  x = XReadByte( marker_address);
  if (x != XLIFO_MARKER) {
    // marker ukazuje spatne, najdi novou pozici markeru
    marker_address = XLifoFindMarker();
  }//if
  addr = marker_address;
#else
  addr = XLifoFindMarker();     // vzdy hledej marker
#endif
  if (addr == XLIFO_INVALID_ADDRESS) {
    return NO;          // znacka nenalezena, resp. nepodarilo se nove LIFO
  }//if
  // Kontrola, zda je v LIFO misto na novy zaznam
  if (addr + sizeof(TXLifoData) >= XLIFO_NEXT_ADDRESS) {
    // Plne LIFO, u LIFO nemuzu prepisovat a zapis tak nelze provest
    return NO;
  }//if
  // Novy zaznam se do LIFO vleze
  // Zapis dat, krome prvniho bytu
  if (!WriteData(addr)) {
    return( NO);          // zapis dat se nepovedl
  }//if
#ifdef XLIFO_VERIFY
  // verifikace dat, krome prvniho bytu
  if (!VerifyData(addr)) {
    return NO;          // zapis dat se nepovedl
  }//if
#endif
  // novy marker :
  // zapis markeru na nasledujici pozici LIFO
  x = XReadByte(addr);  // stary marker
  if (!XWriteByte(addr + sizeof(TXLifoData), x)) {
    return NO;
  }//if
#ifdef XLIFO_VERIFY
  // verifikace markeru :
  if (XReadByte(addr  + sizeof(TXLifoData)) != x) {
    return NO;         // nezapsal se novy marker
  }//if
#endif
#ifdef XLIFO_FAST
  marker_address = addr + sizeof(TXLifoData);
#endif
  // prepsani stareho markeru novymi daty :
  x = XLifoArray[0];         // prvni byte komunikacni struktury
  if (!XWriteByte(addr, x)) {
    return NO;               // zustal stary marker
  }//if
#ifdef XLIFO_VERIFY
  if (XReadByte(addr) != x) {
    return NO;            // zustal stary marker
  }//if
#endif
  return YES;
} // XLifoWrite

#ifdef XLIFO_READ
//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

void XLifoRead(word address) {
// Naplni komunikacni strukturu daty z <address>
  byte i;
  XBlockReadStart(address);
  for (i = 0; i < sizeof(TXLifoData); i++) {
    XLifoArray[i] = XBlockReadData(address++);
  }//for
  XBlockReadStop();
} // XLifoRead
#endif // XLIFO_READ

//-----------------------------------------------------------------------------
// Hledani Markeru
//-----------------------------------------------------------------------------

word XLifoFindMarker() {
// Vrati adresu markeru
word i;
byte x;
byte found;

  found = NO;
  for (i = XLIFO_START; i < XLIFO_NEXT_ADDRESS; i += sizeof(TXLifoData)) {
    x = XReadByte(i);
    if (x == XLIFO_MARKER) {
      found = YES;
      break;
    }//if
  }//for
  if (found) {
    return i;
  }//if
  // znacka nenalezena, zaloz prazdne LIFO :
  if (!XLifoReset()) {
    return XLIFO_INVALID_ADDRESS;
  }//if
  return XLIFO_START;
} // XLifoFindMarker

//-----------------------------------------------------------------------------
// Prazdne Lifo
//-----------------------------------------------------------------------------

TYesNo XLifoReset() {
  // Zalozi prazdne LIFO
  // Zaloz prazdne LIFO (marker na prvni adrese) :
  if (!XWriteByte(XLIFO_START, XLIFO_MARKER)) {
    return NO;
  }//if
  if (XReadByte(XLIFO_START) != XLIFO_MARKER) {
    return NO;    // neproslo kontrolni cteni
  }//if
#ifdef XLIFO_FAST
  marker_address = XLIFO_START;
#endif
  return YES;
} // XLifoReset

//-----------------------------------------------------------------------------
// Zapis dat
//-----------------------------------------------------------------------------

static TYesNo WriteData(word addr) {
  // Zapise komunikacni strukturu krome prvniho bytu
  byte i;
  byte new_page;

  addr++;               // vynechavame prvni byte
  new_page = YES;       // start nove stranky
  for (i = 1; i < sizeof(TXLifoData); i++) {
    if (new_page) {
      // zacatek nove stranky
      new_page = NO;
      if (!XPageWriteStart(addr)) {
        return NO;
      }//if
    }//if
    XPageWriteData(addr, XLifoArray[i]);         // zapis dat do stranky
    addr++;                                      // nova adresa
    if ((addr & (EEP_PAGE_SIZE - 1)) == 0) {
      // nova adresa je v nove strance
      XPageWritePerform();                      // Zapis predchozi stranku
      new_page = YES;
    }//if
  }//for
  if (!new_page) {
    XPageWritePerform();                         // zapis zbytek posledni stranky
  }//if
  return YES;
} // WriteData

#ifdef XLIFO_VERIFY
//-----------------------------------------------------------------------------
// Verifikace dat
//-----------------------------------------------------------------------------

static TYesNo VerifyData(word addr) {
  // Porovna data zapsana v EEPROM s komunikacni strukturou (vynecha prvni byte)
  byte i;

  if (sizeof(TXLifoData) == 1) {
    return YES;           // prvni byte se vynechava, hotovo
  }//if
  // vynechavame prvni byte :
  addr++;
  XBlockReadStart(addr);
  for (i = 1; i < sizeof( TXLifoData); i++) {
    if (XBlockReadData(addr++) != XLifoArray[i]) {
      XBlockReadStop();
      return( NO);         // spatne zapsany byte
    }//if
  }//for
  XBlockReadStop();
  return YES;
} // VerifyData

#endif // XLIFO_VERIFY
