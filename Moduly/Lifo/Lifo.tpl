//*****************************************************************************
//
//    Lifo.tpl - LIFO template module
//    Version 1.0, Petr
//
//*****************************************************************************

// LIFO znamena pamet, do ktere lze zapisovat. Pokud je LIFO cela zaplnena, nelze jiz pridavat dalsi zaznamy.
// Z LIFO lze vymazat posledni ulozeny zaznam, pripadne smazat celou LIFO naraz.

// Data jsou ukladana po zaznamech TLifoData.
// Za poslednim zaznamem je zaznam - marker, ktery ma na prvnim bytu smluveny znak LIFO_MARKER.
// LIFO zacina na adrese LIFO_START_ADDRESS, prvni volna adresa za LIFO je LIFO_NEXT_ADDRESS

#define XLIFO_INVALID_ADDRESS 0xFFFF                                 // neplatna adresa do LIFO
#define XLIFO_NEXT_ADDRESS    (XLIFO_CAPACITY * sizeof( TXLifoData)) // prvni volna adresa za LIFO

#define XLIFO_BAD_INDEX 0xFFFF           // Neplatny index aktualniho zaznamu v pripade, ze je LIFO prazdne

// Komunikacni struktura pretypovana na pole :

#define XLifoArray     ((byte __xdata__ *)&XLifoData)    // umozni indexovat po bytu

#ifdef XLIFO_FAST
  static word __xdata__ MarkerIndex;                      // Index zaznamu s markerem
#endif

static word __xdata__ Index;       // Index aktualniho zaznamu (zacina od 0), pouze pro vnitrni pouziti, proto static



bit XLifoReset();



//-----------------------------------------------------------------------------
// Hledani markeru
//-----------------------------------------------------------------------------
unsigned int XLifoFindMarker() {
  // Vrati index zaznamu, na kterem je marker. Pokud marker nenajde, zalozi prazdne LIFO. Pokud se i to nepovede,
  // vrati 0.
  unsigned int i;
  // Projedu vsechny prvky a hledam marker
  for (i=0; i<XLIFO_CAPACITY; i++) {
    if (XReadByte(XLIFO_START + i*sizeof(TXLifoData))==XLIFO_MARKER) {
      // Nasel jsem marker
      return i;  // Vratim index markeru
    }//if
  }//for
  // Pokud jsem dosel az sem, nenasel jsem znacku, takze zalozim prazdne LIFO
  if (!XLifoReset()) {
    return 0;
  }//if
  // Zalozil jsem prazdne LIFO, marker je nyni na indexu 0
  return 0;
}

//-----------------------------------------------------------------------------
// Zapis dat
//-----------------------------------------------------------------------------
static bit WriteData(word addr) {
  // Zapise komunikacni strukturu krome prvniho bytu
  byte i;
  byte new_page;

  addr++;               // vynechavame prvni byte
  new_page = 1;       // start nove stranky
  for (i = 1; i < sizeof(TXLifoData); i++) {
    if (new_page) {
      // zacatek nove stranky
      new_page = 0;
      if (!XPageWriteStart(addr)) {
        return 0;
      }//if
    }//if
    XPageWriteData(addr, XLifoArray[i]);         // zapis dat do stranky
    addr++;                                      // nova adresa
    if ((addr & (EEP_PAGE_SIZE - 1)) == 0) {
      // nova adresa je v nove strance
      XPageWritePerform();                      // Zapis predchozi stranku
      new_page = 1;
    }//if
  }//for
  if (!new_page) {
    XPageWritePerform();                         // zapis zbytek posledni stranky
  }//if
  return 1;
} // WriteData

#ifdef XLIFO_VERIFY
//-----------------------------------------------------------------------------
// Verifikace dat
//-----------------------------------------------------------------------------
static bit VerifyData(word addr) {
  // Porovna data zapsana v EEPROM s komunikacni strukturou (vynecha prvni byte)
  byte i;

  if (sizeof(TXLifoData) == 1) {
    return 1;           // prvni byte se vynechava, hotovo
  }//if
  // vynechavame prvni byte :
  addr++;
  XBlockReadStart(addr);
  for (i = 1; i < sizeof( TXLifoData); i++) {
    if (XBlockReadData(addr++) != XLifoArray[i]) {
      XBlockReadStop();
      return( 0);         // spatne zapsany byte
    }//if
  }//for
  XBlockReadStop();
  return 1;
} // VerifyData

#endif // XLIFO_VERIFY


//-----------------------------------------------------------------------------
// Aktualni pozice v LIFO
//-----------------------------------------------------------------------------
#ifdef LIFO_GET_INDEX
unsigned int LifoGetIndex() {
  return Index;
}
#endif

//-----------------------------------------------------------------------------
// Pocet prvku v LIFO
//-----------------------------------------------------------------------------
#ifdef LIFO_COUNT
unsigned int XLifoCount() {
  // Vrati pocet prvku v LIFO
#ifdef XLIFO_FAST
  return MarkerIndex;
#else
  return XLifoFindMarker();
#endif
}
#endif


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------
void XLifoInit() {
  // Inicializuje modul
  // Inicializuju EEPROM
  XInit();
  // Najdu koncovy marker
#ifdef XLIFO_FAST
  // Pokud je XLIFO_FAST, ulozim index markeru do promenne, abych to pak nemusel vyhledavat
  MarkerIndex=XLifoFindMarker();
  // Pokud jsou v LIFO nejake zaznamy, nastavim aktualni prvek na prvni zaznam
  if (MarkerIndex==0) Index=XLIFO_BAD_INDEX; else Index=0;
#else
  // Pokud jsou v LIFO nejake zaznamy, nastavim aktualni prvek na prvni zaznam
  if (XLifoFindMarker()==0) Index=XLIFO_BAD_INDEX; else Index=0;
#endif
}


//-----------------------------------------------------------------------------
// Pridani zaznamu do LIFO
//-----------------------------------------------------------------------------
bit XLifoAdd() {
  // Zapise globalni komunikacni strukturu do LIFO
  byte x;
  word Adresa;   // Adresa v EEPROM, na kterou se prave zapisuje

#ifdef XLIFO_FAST
  // Marker by mel byt na prvnim bajtu zaznamu s indexem MarkerIndex. Zkontroluju, zda tam je.
  x=XReadByte(MarkerIndex*sizeof(TXLifoData));
  if (x!=XLIFO_MARKER) {
    // Marker ukazuje spatne, najdu novou pozici markeru
    MarkerIndex=XLifoFindMarker();
  }//if
  Index=MarkerIndex;
#else
  Index=XLifoFindMarker();     // Vzdy hledej marker
#endif
  // V Index mam nyni index zaznamu s markerem, tj. index nove zalozeneho zaznamu
  // Kontrola, zda je v LIFO misto na novy zaznam - za zapsany zaznam musim dostat jeste marker.
  if (Index+1 >= XLIFO_CAPACITY) {
    // Plne LIFO, u LIFO nemuzu prepisovat a zapis tak nelze provest
    return 0;
  }//if
  // Novy zaznam se do LIFO vleze
  // Vypoctu si fyzickou adresu v EEPROM, abych to nemusel vzdy znovu pocitat
  Adresa=XLIFO_START+Index*sizeof(TXLifoData);
  // Zapis dat, krome prvniho bytu
  if (!WriteData(Adresa)) {
    return 0;          // Zapis dat se nepovedl
  }//if
#ifdef XLIFO_VERIFY
  // Verifikace dat, krome prvniho bytu
  if (!VerifyData(Adresa)) {
    return 0;          // Zapis dat se nepovedl
  }//if
#endif
  // Novy marker
  // Zapis markeru na nasledujici pozici LIFO
  Adresa+=sizeof(TXLifoData);
  if (!XWriteByte(Adresa, XLIFO_MARKER)) {
    return 0;
  }//if
#ifdef XLIFO_VERIFY
  // Verifikace markeru :
  if (XReadByte(Adresa) != XLIFO_MARKER) {
    return 0;         // Nezapsal se novy marker
  }//if
#endif
#ifdef XLIFO_FAST
  MarkerIndex++;
#endif
  // Prepsani stareho markeru prvnim bajtem novych dat
  Adresa-=sizeof(TXLifoData);
  if (!XWriteByte(Adresa, XLifoArray[0])) {
    return 0;               // Zustal stary marker
  }//if
#ifdef XLIFO_VERIFY
  if (XReadByte(Adresa) != XLifoArray[0]) {
    return 0;            // Zustal stary marker
  }//if
#endif
  return 1;
}


//-----------------------------------------------------------------------------
// Cteni zaznamu z pozice Index
//-----------------------------------------------------------------------------
#ifdef LIFO_READ
bit XLifoRead() {
  // Naplni komunikacni strukturu daty ze zaznamu na pozici Index
  byte i;
  unsigned int Adresa;

  if (Index==XLIFO_BAD_INDEX) return 0;   // Lifo zadny zaznam neobsahuje
  Adresa=XLIFO_START+Index*sizeof(TXLifoData);
  XBlockReadStart(Adresa);
  for (i=0; i<sizeof(TXLifoData); i++) {
    XLifoArray[i]=XBlockReadData(Adresa);
    Adresa++;
  }//for
  XBlockReadStop();
  return 1;
}
#endif

//-----------------------------------------------------------------------------
// Presun na prvni zaznam
//-----------------------------------------------------------------------------
#ifdef LIFO_FIRST
bit XLifoFirst() {
  // Pokud neni ve LIFO zadny zaznam, nemuzu prejit na prvni zaznam
  if (XLifoCount()==0) {
    Index=XLIFO_BAD_INDEX;  // Zadny zaznam v Lifo neni
    return 0;
  }
  Index=0;
  return 1;
}
#endif

//-----------------------------------------------------------------------------
// Presun na dalsi zaznam
//-----------------------------------------------------------------------------
#ifdef LIFO_NEXT
bit XLifoNext() {
  // Pokud uz jsem na poslednim zaznamu nebo neni ve LIFO zadny zaznam, nemuzu prejit na dalsi zaznam
  if (Index+1 >= XLifoCount()) return 0;
  Index++;
  return 1;
}
#endif

//-----------------------------------------------------------------------------
// Presun na predchozi zaznam
//-----------------------------------------------------------------------------
#ifdef LIFO_PREV
bit XLifoPrev() {
  // Pokud uz jsem na prvnim zaznamu nebo neni ve LIFO zadny zaznam, nemuzu prejit na predchozi zaznam
  if (Index==0 || XLifoCount()==0) return 0;
  Index--;
  return 1;
}
#endif

//-----------------------------------------------------------------------------
// Presun na posledni zaznam
//-----------------------------------------------------------------------------
#ifdef LIFO_LAST
bit XLifoLast() {
  unsigned int Pocet;
  // Pokud neni ve LIFO zadny zaznam, nemuzu prejit na posledni zaznam
  Pocet=XLifoCount();
  if (Pocet==0) return 0;
  Index=Pocet-1;
  return 1;
}
#endif

//-----------------------------------------------------------------------------
// Smaze cele Lifo
//-----------------------------------------------------------------------------
bit XLifoReset() {
  // Zalozi prazdne LIFO
  // Zaloz prazdne LIFO (marker na prvni adrese) :
  if (!XWriteByte(XLIFO_START, XLIFO_MARKER)) {
    return 0;
  }//if
  if (XReadByte(XLIFO_START) != XLIFO_MARKER) {
    return 0;    // neproslo kontrolni cteni
  }//if
#ifdef XLIFO_FAST
  MarkerIndex = 0;  // Chci index zaznamu, ne absolutni adresu
#endif
  Index=XLIFO_BAD_INDEX;  // Zadny zaznam v Lifo neni
  return 1;
} // XLifoReset

//-----------------------------------------------------------------------------
// Smazani posledniho zaznamu
//-----------------------------------------------------------------------------
#ifdef LIFO_DELETE_LAST
bit XLifoDeleteLast() {
  unsigned int Adresa;
  // Prejdu na posledni zaznam
  if (!XLifoLast()) return 0;
  // Zapisu na misto posledniho zaznamu marker, tim posledni zaznam smazu a posunu marker o 1 pozici dozadu
  Adresa=XLIFO_START+Index*sizeof(TXLifoData);
  // Zapis markeru
  if (!XWriteByte(Adresa, XLIFO_MARKER)) {
    return 0;
  }//if
#ifdef XLIFO_VERIFY
  // Verifikace markeru :
  if (XReadByte(Adresa) != XLIFO_MARKER) {
    return 0;         // Nezapsal se novy marker
  }//if
#endif
#ifdef XLIFO_FAST
  MarkerIndex--;
#endif
  // Posunu index aktualniho zaznamu na posledni zaznam. Pokud jsem smazal posledni zaznam a zadny tam uz neni, nastavim index na BAD
  if (Index>0) Index--; else Index=XLIFO_BAD_INDEX;
  return 1;
}
#endif
