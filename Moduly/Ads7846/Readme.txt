            TOUCH SCREEN README



Ads7846.h, c51
--------------------------------------------------------------------------------

TYesNo TouchInit( void)

Inicializace Touch screen

TYesNo TouchIsPressed( void)

Vraci YES, je-li stisknuto,
pokud nadale drzi stisknuto, vraci NO. Teprve po uvolneni
a novem stisknuti vraci YES (funguje na nabeznou hranu
stisknuti)

word TouchX( void)

Vraci souradnici X dotyku v pixelech.
Po prekladu muze vracet napeti v LSB viz dale

word TouchY( void)

Vraci souradnici Y dotyku v pixelech
Po prekladu muze vracet napeti v LSB viz dale

word TouchAux( void)

Mereni napeti na vstupu AUX v LSB prevodniku

word TouchVbat( void);

Mereni napeti na vstupu Vbat v LSB prevodniku

--------------------------------------------------------------------------------
 Komentar k parametrum :

TOUCH_DELAY    - zpozdeni v ms mezi zapnutim napajeni TouchScreen
                 a navzorkovanim hodnoty (podle katalogu < 15ms)

TOUCH_PEN_RISE - doba, po kterou musi byt nejmene stisknut
                 TouchScreen, aby bylo vyhodnoceno stisknuti
                 funkci TouchIsPressed()

TOUCH_PEN_FALL - doba, po kterou muze odpadnout stisknuti,
                 aniz by bylo vyhodnoceno uvolneni stisknuti

TOUCH_DATA_SIZE - rozliseni A/D prevodniku 8 nebo 12 bitu

Rozliseni obrazovky v pixelech :

TOUCH_X_RANGE vodorovne
TOUCH_Y_RANGE svisle

TOUCH_XL  surova hodnota mereni pro vodorovny pixel 0
TOUCH_XR  surova hodnota mereni pro vodorovny pixel (TOUCH_X_RANGE - 1)

TOUCH_YU  surova hodnota mereni pro svisly pixel 0
TOUCH_YD  surova hodnota mereni pro svisly pixel (TOUCH_Y_RANGE - 1)

Kalibrace TouchScreen :

1. Zadejte TOUCH_X_RANGE = 0 a TOUCH_Y_RANGE = 0.
   Tim se vyradi prepocet napeti na pixely - prekompilujte modul

2. Dotknete se TouchScreenu na souradnici 0, 0 pixelu
   a odectete zmerene hodnoty TouchX a TouchY.
   Jsou to cisla TOUCH_XL a TOUCH_YU

3. Dotknete se TouchScreenu na souradnici
   TOUCH_X_RANGE - 1, TOUCH_Y_RANGE - 1.
   Zmerene hodnoty jsou TOUCH_XR a TOUCH_YD

4. Zadejte spravne TOUCH_X_RANGE a TOUCH_Y_RANGE
   a zmerene hodnoty. Po kompilaci vraci funkce
   TouchX, TouchY souradnice v pixelech
