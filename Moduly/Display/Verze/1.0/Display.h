//*****************************************************************************
//
//    Display.h  -  Zobrazeni
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Display_H__
   #define __Display_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


// Pro dialog:
byte CekejNaEnterEsc();
void CekejNaEnter();

// Stringy, ktere musi definovat hlavni program
// Pro dialog:
extern byte code STR_OK[];
extern byte code STR_ZRUSIT[];
extern byte code STR_KONEC[];
extern byte code STR_DALE[];
extern byte code STR_ANO[];
extern byte code STR_NE[];
extern byte code STR_ZNOVU[];

// Symboly, ktere musi definovat hlavni program
// Pro dialog:
extern byte code SYMBOLENTER[];
extern byte code SYMBOLESC[];
// Pro menu:
extern byte code SYMBOLSIPKA[];

// Typ pro predani tlacitek do fce Dialog()
#define KLAVESY_DIALOG_NECEKAT 0x80    // Pokud je nastavene MSB v TKlavesyDialogu, jen vykreslim a necekam na stisk tlacitka - vhodne napr. pro editaci atd.
typedef enum {
  // Zadne klavesy
  KLAVESY_DIALOGU_ZADNE,   // Zadne klavesy a neceka na stisk tlacitka, hned odejde
  // Jedna klavesa
  _KLAVESY_JEDNAKLAVESA=0x10,    // Odtud zacina 1 klavesa
  KLAVESY_DIALOGU_OK=_KLAVESY_JEDNAKLAVESA,
  KLAVESY_DIALOGU_ZRUSIT,
  KLAVESY_DIALOGU_KONEC,
  KLAVESY_DIALOGU_DALE,
  // Dve klavesy
  _KLAVESY_DVEKLAVESY=0x30,       // Odtud zacinaji 2 klavesy
  KLAVESY_DIALOGU_OKZRUSIT=_KLAVESY_DVEKLAVESY,
  KLAVESY_DIALOGU_ANONE,
  KLAVESY_DIALOGU_ZNOVUZRUSIT
} TKlavesyDialogu;

// Konstanty pro vyber z menu
#define MENU_STISKNUTO_ESC     0xFF   // Tuto hodnotu vrati fce VyberMenu(), pokud stiskne Esc nebo dojde k timeoutu, tj. nevybere zadnou polozku
// Typy pro vyber z menu
typedef struct {
  unsigned char *Polozky;       // Ukazatel na polozky oddelene nulou
  unsigned char PocetPolozek;   // Celkovy pocet polozek
  unsigned char *Nadpis;        // Nadpis podmenu
  unsigned int Zobrazeni;       // Ktere polozky zobrazit a ktere ne (LSB odpovida 1. polozce)
  unsigned char VybranaPolozka; // Aktualne vybrana polozka v menu
  unsigned char PrvniZobrazenaPolozka;  // Menu, ktere je zobrazeno na 1. radku
} TPolozkyMenu;

typedef struct {
  unsigned char VybranaPolozka; // Aktualne vybrana polozka v menu
  unsigned char PrvniZobrazenaPolozka;  // Menu, ktere je zobrazeno na 1. radku
} TPolohaVMenu;



//-----------------------------------------------------------------------------
// Obecne fce
//-----------------------------------------------------------------------------

void DisplayBarGraf(byte X, byte Y, byte Delka, byte Vyska, int Hodnota, int Min, int Max, byte Desetiny);
  // Zobrazi bargraf, Min, Max i Hodnota mohou byt i zaporne


//-----------------------------------------------------------------------------
// Dialog
//-----------------------------------------------------------------------------
byte DisplayDialog(byte *Text, TKlavesyDialogu Klavesy);
  // Vykresli na displeji dialog s textem Text a klavesami Klavesy. V promenne Text znamena \xFF oddelovac radku, \x0 je konec stringu.

//-----------------------------------------------------------------------------
// Jazyky
//-----------------------------------------------------------------------------
byte code *DecodeString(byte code *s, byte Index);
byte code *LangSimpleDecode(byte code *s);
#define LangDecode(s, Pocet) DecodeString(s, Pocet * Jazyk)

//-----------------------------------------------------------------------------
// Menu
//-----------------------------------------------------------------------------
byte DisplayMenu(TPolozkyMenu *PolozkyMenu);
  // Provede vyber z menu
byte DisplayChooseItem(byte X, byte Y, byte code *Polozka, byte Pocet, byte Vybrane);
  // Vybere 1 polozku z urciteho poctu moznych a vrati cislo vybrane polozky. V <Vybrane> je default hodnota zacinajici od nuly.

#endif
