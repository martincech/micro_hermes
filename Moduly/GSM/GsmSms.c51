//******************************************************************************
//
//   GsmSms.c     GSM SMS messaging
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "..\inc\Gsm.h"
#include "..\inc\System.h"   // SysDelay
#include <string.h>

#ifdef GSM_SMS_USE_GSMCTL
   #include "..\inc\GsmCtl.h"   // GSM kontext
#else
   #define GsmReady() YES       // Informaci o pripravenosti GSM modulu nemam, beru ze je stale pripraven
#endif

#ifndef GSM_COM2
   #include "..\inc\Com.h"
#else
   #include "..\inc\Com2sw.h"

   #define ComFlushChars Com2FlushChars
   #define ComRxChar     Com2RxChar
   #define ComRxWait     Com2RxWait
   #define ComTxChar     Com2TxChar
#endif

#include "..\inc\COM_util.h"

#ifdef GSM_SMS_DEBUG
  extern byte __xdata__ SmsDebugValue;
  #define SetDebugValue(Value) SmsDebugValue = Value
#else
  #define SetDebugValue(Value)
#endif

// Preddefinovane retezce :

static char code PduSend[]    = "AT+CMGS=\"+";          // Send SMS + "+<number."<CR>
static char code Prompt[]     = "\r\n> ";               // input prompt
static char code SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static char code PduRead[]    = "AT+CMGR=";             // Read SMS <index><CR>
static char code ReadHeader[] = "\r\n+CMGR: ";          // Header of read message
static char code EmptyRead[]  = ",,0\r\n";              // empty SMS message
static char code ReceivedMsg[]= "REC";                  // REC READ/REC UNREAD status

static char code Delete[]     = "AT+CMGD=";             // Delete SMS <index><CR>

#define CTRL_Z  '\x1A'

#define PDU_FIXED_LENGTH      8        // konstatni delka telegramu bez SMSC
#define TIMESTAMP_LENGTH     14        // delka sekce cas+zona

#define SMS_PROMPT_TIMEOUT   10        // Send prompt timeout      * 0.1s


#define TxString( s) ComFlushChars();ComTxString( s)    // pred vyslanim zrus Rx

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------


TYesNo SmsSend( char xdata *DstNumber, char *Text, byte MessageLength)
// Odeslani zpravy
{
byte   i;
byte   ch;

   if( !GsmReady()){
      SetDebugValue(1);
      return( NO);                     // out of context
   }
   // AT command SMS send :
   TxString( PduSend);
   ComTxXString( DstNumber);           // destination number
   ComTxChar( '"');                    // close string with "
   ComTxChar( '\r');
   // wait for prompt :
   if( !ComRxMatch( Prompt, SMS_PROMPT_TIMEOUT)){
      SetDebugValue(2);
      return( NO);
   }
   // send text :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      // ASCII to GSM alphabet :
      if( ch == '@'){
         ch = '\0';
      }
      ComTxChar( ch);
   }
   ComTxChar( CTRL_Z);                 // message terminator
#ifdef GSM_SMS_USE_GSMCTL
   // Nacteni odpovedi necham na automat
   GsmCtl.Status  = GSM_SMS_SENDING;       // kontext radice
   GsmCtl.Counter = GSM_SMS_SEND_TIMEOUT;  // timeout potvrzeni
#else
   // Automat se nepouziva, pockam na odpoved
   if( !ComRxMatch( SendOk, 10 * GSM_SMS_SEND_TIMEOUT)){        // Timeout se do ComRxMatch() zadava v 0.1sec
      SetDebugValue(3);
      return( NO);
   }
 #endif
   SetDebugValue(0);  // V poradku
   return( YES);
} // SmsSend


//-----------------------------------------------------------------------------
// Send OK
//-----------------------------------------------------------------------------

#ifdef GSM_SMS_USE_GSMCTL

TYesNo SmsSendOk( void)
// Testuje potvrzeni odeslani
{
   if( !ComRxMatch( SendOk, 0)){
      return( NO);
   }
   return( YES);
} // SmsSendOk

#endif

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

byte SmsRead( byte Index, char xdata *SrcNumber, char xdata *Text)
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku
{
byte MessageLength;
byte i;
byte ch;

   if( !GsmReady()){
      SetDebugValue(1);
      return( SMS_READ_ERROR);         // kontextova cinnost, nelze cist
   }
   Index++;                            // cislovano od 1
   // AT prikaz Read :
   TxString( PduRead);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
   if( !ComRxMatch( ReadHeader, 5)){
      SetDebugValue(2);
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   // read first character :
   if( !ComRxChar( &ch)){
      SetDebugValue(3);
      return( SMS_READ_ERROR);
   }
   // test for empty message signature :
   if( ch == '0'){
      if( ComRxMatch( EmptyRead, 1)){
         SetDebugValue(4);
         return( SMS_EMPTY_MESSAGE);
      }
      SetDebugValue(5);
      return( SMS_READ_ERROR);
   }
   // test for message type :
   if( ch != '"'){
      SetDebugValue(6);
      return( SMS_READ_ERROR);
   }
   if( !ComRxMatch( ReceivedMsg, 1)){
      SetDebugValue(7);
      return( SMS_READ_ERROR);
   }
   if( !ComWaitChar( ',')){
      SetDebugValue(8);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // phone number :
   if( !ComRxString( SrcNumber, GSM_MAX_NUMBER)){
      SetDebugValue(9);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   if( SrcNumber[ 0] == '+'){          // remove leading + sign
      ch = strlen( SrcNumber);         // sorry
      for( i = 0; i < ch; i++){
          SrcNumber[ i] = SrcNumber[ i + 1];
      }
   }
   // skip up to end of line :
   if( !ComWaitChar( '\n')){
      SetDebugValue(10);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // text of the message :
   MessageLength = ComRxDelimiter( Text, SMS_MAX_LENGTH, '\r');
   if( !MessageLength){
      SetDebugValue(11);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // transform from GSM alphabet :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      if( ch == '\0'){
         Text[ i] = '@';
      }
   }
   SetDebugValue(0);    // V poradku
   return( MessageLength);
} // SmsRead



//-----------------------------------------------------------------------------
// Delete
//-----------------------------------------------------------------------------


void SmsDelete( byte Index)
// Smaze prijatou zpravu z pozice <Index>
{
   Index++;                            // cislovano od 1
   TxString( Delete);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
} // SmsDelete
