//*****************************************************************************
//
//    X25645.tpl  -  Template for X25645 / AT25640 services
//    Version 1.0, (c) Vymos
//
//
//    // 3.3.2003: Upraveni pro Atmel AT25xxx EEPROM podle jejich zdrojaku. Pred selectem je navic treba nastavit piny na definovanou uroven,
//                 jinak to posouvalo bity. Xicoru ani SGS Thomsonu to nevadilo, ale Atmel nesel. S Xicorem ani SGS jsem to jeste nezkousel.
//
//    // 7.7.2003: Moznost negovani signalu SI a SCK
//
//*****************************************************************************

// Pozor, XSO je vystup z pameti, budeme jej cist
// XSI je vstup do pameti, do nej zapisujeme

// Instrukce X25645/AT25640 :

#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RSDR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data
#define EEP_WRITE   0x02           // Write data

// Instrukce X25645 :

#define EEP_SFLB    0x00           // Set Flag Bit

// Status register format X25645/AT25640 :

#define EEP_MASK_WIP  0x01         // WIP je v 1 probiha-li zapis
#define EEP_MASK_WEL  0x02         // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BL0  0x04         // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BL1  0x08         // ochranny bit 1 (1/2 kapacity)
#define EEP_MASK_WPEN 0x80         // ovladani Write Protect /WP pinu

// Status register format X25645 :

#define EEP_MASK_WD14  0x00        // Watchdog 1.4s
#define EEP_MASK_WD06  0x10        // Watchdog 600ms
#define EEP_MASK_WD02  0x20        // Watchdog 200ms
#define EEP_MASK_WD0   0x30        // Watchdog disabled
#define EEP_MASK_FLB   0x40        // Power up flag

// Lokalni funkce :

static void WrenCmd( void);
// Posle instrukci WREN

static byte RsdrCmd( void);
// Precte a vrati status registr

static void WrsrCmd( byte value);
// Zapise do status registru

static void WipPoll( void);
// Cekani na pripravenost


#define NastavPinyPredSelectem() { XSCK=!XSCK_H; XSI=XSI_H; XSO=1; }

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void XInit() {
  // Nastavi klidove hodnoty na sbernici, inicializuje pamet
  XCS    = 1;                    // deselect
  XSCK   = !XSCK_H;              // vychozi postaveni hodin
  XSI    = XSI_H;                // data out
  XSO    = 1;                    // data in
  // zapis do stavoveho registru :
  WipPoll();
  WrenCmd();                     // Set write enable latch
  WrsrCmd( EEP_MASK_WD0);        // Zrusit watchdog a ochranu pameti
} // XInit

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

#ifdef X_READ_BYTE
byte XReadByte( word address)
// Precte byte z EEPROM <address>
{
byte value;

   XBlockReadStart( address);     // zacatek bloku
   value = XRawReadByte();        // jeden datovy byte
   XStop();                       // konec cteni
   return( value);
} // XReadByte

#endif

//-----------------------------------------------------------------------------
// Strankove cteni z pameti
//-----------------------------------------------------------------------------

void XBlockReadStart( word address)
// Zahaji blokove cteni z EEPROM <address>
{
   WipPoll();                     // pripravenost
   NastavPinyPredSelectem();
   XCS     = 0;                   // select
   XRawWriteByte( EEP_READ);      // READ instruction
   XRawWriteByte( address >> 8);  // high address
   XRawWriteByte( address);       // low address
} // XBlockReadStart

//-----------------------------------------------------------------------------
// Ukonceni
//-----------------------------------------------------------------------------

void XStop( void)
// Ukonci operaci
{
   XSCK  = !XSCK_H;               // Predchozi operace nechava v 1
   _nop_();                       // tcsh > 250ns
   XCS   = 1;                     // deselect
   _nop_();                       // Radsi pockam, aby se opravdu provedl deselect
   // Piny, ktere by se mohly sdilet, musim po deselectu nastavit na 1
   XSCK=XSI=XSO=1;
 } // XStop

#ifdef X_WRITE_BYTE

//-----------------------------------------------------------------------------
// Zapis do pameti
//-----------------------------------------------------------------------------

TYesNo XWriteByte( word address, byte value)
// Zapise byte <value> na <address> v EEPROM
{
   if( !XPageWriteStart( address)){
      return( NO);
   }
   XRawWriteByte( value);         // data
   XStop();                       // provedeni zapisu
   return( YES);                  // necekame na dokonceni zapisu
} // XWriteByte

#endif

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

TYesNo XPageWriteStart( word address)
// Zahaji zapis stranky od <address> v EEPROM
{
   if( !XAccuOk()){
      return( NO);
   }
   WipPoll();
   WrenCmd();                     // Set write enable latch
   NastavPinyPredSelectem();
   _nop_();
   _nop_();
   _nop_();
   XCS=0;                         // select
   XRawWriteByte( EEP_WRITE);     // WRITE instruction
   XRawWriteByte( address >> 8);  // high address
   XRawWriteByte( address);       // low address
   return( YES);
} // XPageWriteStart

//-----------------------------------------------------------------------------
// Instrukce WREN
//-----------------------------------------------------------------------------

static void WrenCmd() {
  // Posle instrukci WREN
  NastavPinyPredSelectem();
  XCS   = 0;                     // select
  XRawWriteByte( EEP_WREN);
  XStop();
} // WrenCmd


//-----------------------------------------------------------------------------
// Instrukce RDSR
//-----------------------------------------------------------------------------

static byte RsdrCmd() {
  // Precte a vrati status registr
  byte value;
  NastavPinyPredSelectem();
  XCS  = 0;                      // select
  XRawWriteByte( EEP_RSDR);
  value  = XRawReadByte();
  XStop();   // !!! TADY je u Atmelu jen CS=1, se SCK se tam nic nedela
  return value;
}  // RsdrCmd

//-----------------------------------------------------------------------------
// Instrukce WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd(byte value) {
  // Zapise do status registru
  NastavPinyPredSelectem();
  XCS  = 0;                      // select
  XRawWriteByte( EEP_WRSR);
  XRawWriteByte( value);
  XStop();
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Cekani na dokonceni zapisu
//-----------------------------------------------------------------------------

static void WipPoll() {
  // Cekani na pripravenost
  while( RsdrCmd() & EEP_MASK_WIP);
} // WipPoll

//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

void XRawWriteByte(byte value) {
  // Zapise byte, po zapise zustane SCK = 1
  byte i;
  for (i=0; i<8; i++) {
    XSCK  = !XSCK_H;
#if (XSI_H==1)
    XSI   = value & 0x80;
#else
    XSI   = !(value & 0x80);
#endif
    value <<= 1;    // Prodleva, vyuziju pro posuv
    XSCK  = XSCK_H;             // strobovani nabeznou hranou
  }
} // XRawWriteByte

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte XRawReadByte() {
  // Precte a vrati byte, po cteni zustane SCK = 1
  byte value=0;
  byte i;
  XSO  = 1;                    // vyrazeni vystupniho budice
  for (i=0; i<8; i++) {
    XSCK  = !XSCK_H;            // strobovaci hrana
    value <<= 1;                // prodleva, vyuziju pro posuv
    if (XSO) value++;           // teprve ted cteme
    XSCK  = XSCK_H;             // neaktivni hrana
  }
  return value;
} // XRawReadByte








