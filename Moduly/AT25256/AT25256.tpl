//*****************************************************************************
//
//    AT25256.tpl  AT25256 EEPROM services template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

// Pozor, XSO je vystup z pameti, budeme jej cist
// XSI je vstup do pameti, do nej zapisujeme

// Instrukce AT25256 :

#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RDSR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data
#define EEP_WRITE   0x02           // Write data

// Status register format AT25256 :

#define EEP_MASK_RDY  0x01         // -READY, je v 1 probiha-li zapis
#define EEP_MASK_WEN  0x02         // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BP0  0x04         // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BP1  0x08         // ochranny bit 1 (1/2 kapacity)
#define EEP_MASK_WPEN 0x80         // ovladani Write Protect /WP pinu

// Lokalni funkce :

static void WrenCmd( void);
// Posle instrukci WREN

static byte RdsrCmd( void);
// Precte a vrati status registr

static void WrsrCmd( byte value);
// Zapise do status registru

static void WaitForReady( void);
// Cekani na pripravenost

// porty :

#define Select()   XCS  = 0
#define Deselect() XCS  = 1
#define SetSCK()   XSCK = XSCK_H
#define ClrSCK()   XSCK = !XSCK_H
#define SetSI()    XSI  = XSI_H
#define ClrSI()    XSI  = !XSI_H
#define GetSO()    XSO
#define EnableSO() XSO  = 1


// Urovne pred povolenim CS :
#define SetupCS()   ClrSCK(); EnableSO();
// Urovne po ukonceni operace :
#define CleanupCS() XSCK=1; XSI=1; XSO=1;       // vsechny porty do H

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void XInit()
// Nastavi klidove hodnoty na sbernici, inicializuje pamet
{
  Deselect();                    // deselect CS
  SetupCS();                     // vychozi urovne
  // zapis do stavoveho registru :
  WaitForReady();
  WrenCmd();                     // Set write enable latch
  WrsrCmd( 0);                   // Zrusit ochranu pameti
  CleanupCS();                   // uklid urovni
} // XInit

#ifdef X_READ_BYTE
//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

byte XReadByte( word address)
// Precte byte z EEPROM <address>
{
byte value;

   XBlockReadStart( address);     // zacatek bloku
   value = XRawReadByte();        // jeden datovy byte
   XStop();                       // konec cteni
   return( value);
} // XReadByte

#endif // X_READ_BYTE

//-----------------------------------------------------------------------------
// Strankove cteni z pameti
//-----------------------------------------------------------------------------

void XBlockReadStart( word address)
// Zahaji blokove cteni z EEPROM <address>
{
   SetupCS();                     // urovne pred CS
   WaitForReady();                // pripravenost
   Select();                      // select CS
   XRawWriteByte( EEP_READ);      // READ instruction
   XRawWriteByte( address >> 8);  // high address
   XRawWriteByte( address);       // low address
} // XBlockReadStart

//-----------------------------------------------------------------------------
// Ukonceni
//-----------------------------------------------------------------------------

void XStop( void)
// Ukonci operaci
{
   Deselect();                    // deselect CS
   CleanupCS();
 } // XStop

#ifdef X_WRITE_BYTE
//-----------------------------------------------------------------------------
// Zapis do pameti
//-----------------------------------------------------------------------------

TYesNo XWriteByte( word address, byte value)
// Zapise byte <value> na <address> v EEPROM
{
   if( !XPageWriteStart( address)){
      return( NO);
   }
   XRawWriteByte( value);         // data
   XStop();                       // provedeni zapisu
   return( YES);                  // necekame na dokonceni zapisu
} // XWriteByte

#endif // X_WRITE_BYTE

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

TYesNo XPageWriteStart( word address)
// Zahaji zapis stranky od <address> v EEPROM
{
   if( !XAccuOk()){
      return( NO);
   }
   SetupCS();                     // urovne pred CS
   WaitForReady();                // cekani na pripravenost
   WrenCmd();                     // Set write enable latch
   Select();                      // select CS
   XRawWriteByte( EEP_WRITE);     // WRITE instruction
   XRawWriteByte( address >> 8);  // high address
   XRawWriteByte( address);       // low address
   return( YES);
} // XPageWriteStart

//-----------------------------------------------------------------------------
// Instrukce WREN
//-----------------------------------------------------------------------------

static void WrenCmd()
// Posle instrukci WREN
{
   Select();                       // select CS
   XRawWriteByte( EEP_WREN);
   Deselect();                     // deselect CS
} // WrenCmd


//-----------------------------------------------------------------------------
// Instrukce RDSR
//-----------------------------------------------------------------------------

static byte RdsrCmd()
// Precte a vrati status registr
{
byte value;

   Select();                      // select CS
   XRawWriteByte( EEP_RDSR);
   value  = XRawReadByte();
   Deselect();                    // deselect CS
   return( value);
}  // RdsrCmd

//-----------------------------------------------------------------------------
// Instrukce WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte value)
// Zapise do status registru
{
   Select();                      // select CS
   XRawWriteByte( EEP_WRSR);
   XRawWriteByte( value);
   Deselect();                    // deselect CS
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Cekani na dokonceni zapisu
//-----------------------------------------------------------------------------

static void WaitForReady()
// Cekani na pripravenost
{
   while( RdsrCmd() & EEP_MASK_RDY);
} // WaitForReady

//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

void XRawWriteByte( byte Value)
// Zapise byte
{
byte i;

   i = 8;
   do {
      if( Value & 0x80) {
         SetSI();
      } else {
         ClrSI();
      }
      SetSCK();
      Value <<= 1;
      ClrSCK();
   } while( --i);
} // XRawWriteByte

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte XRawReadByte()
// Precte a vrati byte
{
byte Value = 0;
byte i = 8;

   EnableSO();                // vyrazeni vystupniho budice
   do {
      SetSCK();
      Value <<= 1;
      if( GetSO()){
         Value |= 1;
      }
      ClrSCK();
   } while( --i);
   return( Value);
} // XRawReadByte








