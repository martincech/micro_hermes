;------------------------------------------------------------------------------
;
; wbin2bcd.A51 generated from: BCD.C51
;
;------------------------------------------------------------------------------


NAME    DBIN2BCD

?PR?_dbin2bcd?DBIN2BCD    SEGMENT CODE
	PUBLIC	_dbin2bcd

; //-----------------------------------------------------------------------------
; // dbin2bcd
; //-----------------------------------------------------------------------------

; dword dbin2bcd( dword x) //  R4:R5:R6:R7

        RSEG  ?PR?_dbin2bcd?DBIN2BCD
	USING	0
_dbin2bcd:
			; SOURCE LINE # 82
; // prevede 0..99 999 999 do bcd
; {
			; SOURCE LINE # 84
	  mov     r3, ar7         ; X - zdrojovy operand R0:R3
	  mov     r2, ar6
	  mov     r1, ar5
	  mov     r0, ar4

	  clr     a               ; nuluj vysledek
	  mov     r7, a
	  mov     r6, a
	  mov     r5, a
	  mov     r4, a

	  call    bcdbyte         ; zpracuj byte MSB (r0)
	  mov     r0, ar1
	  call    bcdbyte
	  mov     r0, ar2
	  call    bcdbyte
	  mov     r0, ar3
	  call    bcdbyte         ; zpracuj byte LSB (r3)
	  ret                     ; konec funkce
bcdbyte :
	  mov     b, #8           ; vsechny bity zdrojoveho registru
bcd00e  :
	  mov     a, r0           ; posun doleva, nastav C podle MSB
	  rlc     a
	  mov     r0, a

	  mov     a, r7
	  addc    a, r7           ; vynasob 2* pricti rotovany bit
	  da      a
	  mov     r7, a

	  mov     a, r6
	  addc    a, r6           ; vynasob 2* pricti prenos
	  da      a
	  mov     r6, a

	  mov     a, r5
	  addc    a, r5           ; vynasob 2* pricti prenos
	  da      a
	  mov     r5, a

	  mov     a, r4
	  addc    a, r4           ; vynasob 2* pricti prenos
	  da      a
	  mov     r4, a

	  djnz    b,bcd00e
	  ret
; } // dbin2bcd
			; SOURCE LINE # 135
	RET

        END
