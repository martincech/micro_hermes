;------------------------------------------------------------------------------
;
; wbcdsub.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    WBCDSUB

?PR?_wbcdsub?WBCDSUB     SEGMENT CODE

        PUBLIC  _wbcdsub

; //-----------------------------------------------------------------------------
; // wbcdsub
; //-----------------------------------------------------------------------------
;
; word wbcdsub( word x /*R6:R7*/, word y /*R4:R5*/)

        RSEG  ?PR?_wbcdsub?WBCDSUB
	USING	0
_wbcdsub:
			; SOURCE LINE # 307
; // vrati x-y
; {
			; SOURCE LINE # 309
	  clr c
	  mov a, #9AH                     ; doplnek
	  subb a, r5                      ; inverze Y
	  add a, r7                       ; plus X
	  da  a                           ; BCD korekce
	  mov r7, a                       ; navrat LSB
	  mov a, #99H
	  addc a, #0                      ; prenos z dolniho
	  subb a, r4                      ; doplnek MSB Y
	  add  a, r6                      ; plus MSB X
	  da a                            ; BCD korekce
	  mov r6, a                       ; navrat MSB
; } // wbcdsub
			; SOURCE LINE # 324
	RET

        END
