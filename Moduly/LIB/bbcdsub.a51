;------------------------------------------------------------------------------
;
; bbcdsub.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    BBCDSUB

?PR?_bbcdsub?BBCDSUB     SEGMENT CODE

        PUBLIC  _bbcdsub

; //-----------------------------------------------------------------------------
; // bbcdsub
; //-----------------------------------------------------------------------------
;
; byte bbcdsub( byte x, byte y)

        RSEG  ?PR?_bbcdsub?BBCDSUB
	USING	0
_bbcdsub:
			; SOURCE LINE # 289
; // vrati x-y
; {
			; SOURCE LINE # 291
	  clr c
	  mov a, #9AH                     ; doplnek
	  subb a, r5                      ; inverze Y
	  add a, r7                       ; plus X
	  da  a                           ; BCD korekce
	  mov r7, a                       ; navrat
; } // bbcdsub
			; SOURCE LINE # 301
	RET

        END

