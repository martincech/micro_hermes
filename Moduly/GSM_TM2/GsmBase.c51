//******************************************************************************
//
//   GsmBase.c    GSM basic services (Teltonika TM2)
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "..\inc\Gsm.h"
#include "..\inc\System.h"   // SysDelay

#ifndef GSM_COM2
   #include "..\inc\Com.h"
#else
   #include "..\inc\Com2sw.h"

   #define ComFlushChars Com2FlushChars
   #define ComRxChar     Com2RxChar
   #define ComRxWait     Com2RxWait
   #define ComTxChar     Com2TxChar
#endif

#include "..\inc\COM_util.h"

// Preddefinovane retezce :

static char code Ok[]          = "\r\nOK\r\n";          // ok reply
static char code EchoOff[]     = "ATE0\r";              // echo off
static char code ErrorMode[]   = "ATV1+CMEE=1\r";       // Extended error mode
static char code TextMode[]    = "AT+CMGF=1\r";         // SMS text mode
static char code SendParams1[] = "AT+CSMP=17,";         // SMS send parameters part 1 + <expiration>
static char code SendParams2[] = ",0,0\r";              // SMS send parameters part 2


#define TxString( s) ComFlushChars();ComTxString( s)    // pred vyslanim zrus Rx
#define TO_REPLY          8                             // timeout odpovedi [100ms]
#define TO_MEMORY_SWITCH 50                             // timeout pri prepnuti pameti [100ms]

#ifdef GSM_DEBUG
   byte __xdata__ GsmErrno;
   #define TRACE( n)  GsmErrno = n;
#else
   #define TRACE( n)
#endif

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo GsmReset( void)
// Inicializace modemu
{
   // Echo off :
   TxString( EchoOff);
   if( !ComRxWait( TO_REPLY)){         // cekani na odpoved
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // pred OK
      TRACE( 2);
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // za OK
      TRACE( 3);
      return( NO);
   }
   // Extended error mode :
   ComTxString( ErrorMode);
   if( !ComRxMatch( Ok, TO_REPLY)){
      TRACE( 4);
      return( NO);
   }
   // SMS mode :
   ComTxString( TextMode);
   if( !ComRxMatch( Ok, TO_REPLY)){
      TRACE( 5);
      return( NO);
   }
   // SMS Expiration :
   TxString( SendParams1);
   ComTxDec( SMS_EXPIRATION);
   ComTxString( SendParams2);
   if( !ComRxMatch( Ok, TO_REPLY)){
      TRACE( 6);
      return( NO);
   }
   // Predpoklada se, ze GSM modul je jiz spravne nastaven na pamet v SIM karte
   TRACE( 0);
   return( YES);
} // GsmReset

//******************************************************************************
// Registrace
//******************************************************************************

static char code Registered[] = "AT+CREG?\r";           // Registration
static char code RegHeader[]  = "\r\n+CREG: ";          // Header of registration


TYesNo GsmRegistered( void)
// Vraci YES, je-li modem registrovan v siti
{
byte Value;

   TxString( Registered);
   if( !ComRxMatch( RegHeader, TO_REPLY)){
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <n>
      TRACE( 2);
      return( NO);
   }
#ifdef GSM_TM2_OLD
   if( !ComRxChar( &Value)){           // Tm2 stara verze - mezera navic
      TRACE( 3);
      return( NO);
   }
#endif
   Value = ComRxDec();                 // pole <stat>
   if( Value == 1 || Value == 5){
      TRACE( 0);
      return( YES);                    // registered or roaming
   }
   TRACE( 4);
   return( NO);
} // GsmRegistered

#ifdef GSM_OPERATOR
//******************************************************************************
// Operator
//******************************************************************************

static char code Operator[]   = "AT+COPS?\r";           // Get operator
static char code OperHeader[] = "\r\n+COPS: ";          // operator header

TYesNo GsmOperator( char xdata *Name)
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1
{
   TxString( Operator);
   if( !ComRxMatch( OperHeader, TO_REPLY)){
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <mode>
      TRACE( 2);
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <format>
      TRACE( 3);
      return( NO);
   }
   if( !ComRxString( Name, SMS_MAX_OPERATOR)){
      TRACE( 4);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // GsmOperator

#endif // GSM_OPERATOR

#ifdef GSM_SIGNAL_STRENGTH
//******************************************************************************
// Sila signalu
//******************************************************************************

static char code Rssi[]       = "AT+CSQ\r";             // Get Received Signal Strength
static char code RssiHeader[] = "\r\n+CSQ: ";           // Signal strength header

byte GsmSignalStrength( void)
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit
{
byte Value;

   TxString( Rssi);
   if( !ComRxMatch( RssiHeader, TO_REPLY)){
      TRACE( 1);
      return( 99);
   }
   Value = ComRxDec();                 // relativni sila
   TRACE( 0);
   return( Value);
} // GsmSignalStrength

#endif // GSM_SIGNAL_STREGTH

#ifdef GSM_SMS_MEMORY
//-----------------------------------------------------------------------------
// Set SMS Memory
//-----------------------------------------------------------------------------

static char code MemoryPhone[] = "AT+CPMS=\"MT\",\"MT\",\"MT\"\r";  // SMS data in the internal memory
static char code MemorySim[]   = "AT+CPMS=\"SM\",\"SM\",\"SM\"\r";  // SMS data in the SIM memory
static char code MemoryHeader[]= "\r\n+CPMS: ";         // Header of memory settings

TYesNo SmsMemory( byte Location)
// Nastaveni pameti SMS. <Location> viz TSmsMemory
{
   switch( Location){
      case SMS_SIM_MEMORY :
         TxString( MemorySim);
         break;
      case SMS_PHONE_MEMORY :
         TxString( MemoryPhone);
         break;
      default :
         TRACE( 5);
         return( NO);
   }
   if( !ComRxMatch( MemoryHeader, TO_MEMORY_SWITCH)){ // prepnuti trva az nekolik sekund
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( '\r')){            // preskoc na konec radku
      TRACE( 2);
      return( NO);
   }
   if( !ComWaitChar( '\n')){
      TRACE( 3);
      return( NO);
   }
   if( !ComRxMatch( Ok, TO_REPLY)){    // OK
      TRACE( 4);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // SmsMemory

#endif // GSM_SMS_MEMORY
