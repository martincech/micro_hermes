//*****************************************************************************
//
//   MBAscii.h   Modbus serial communication - ASCII packets
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBAscii_H__
   #define __MBAscii_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
//   Rx
//------------------------------------------------------------------------------

TYesNo MBAReceive( void **Pdu, word *Size);
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>

byte MBAGetAddress( void);
// Vrati adresu paketu. POZOR, volat az po MBAReceive

//------------------------------------------------------------------------------
//   Tx
//------------------------------------------------------------------------------

void MBAWriteAddress( byte Address);
// Zapise adresu paketu

void MBAWriteByte( byte Data);
// Zapise 8bitove slovo do paketu

void MBAWriteWord( word Data);
// Zapise 16bitove slovo do paketu

void MBAWriteCrc( void);
// Spocita a zapise zabezpeceni

#endif
