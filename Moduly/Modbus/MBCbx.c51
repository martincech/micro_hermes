//******************************************************************************
//
//   MBCb.c          Modbus callback definitions TEMPLATE
//   Version 1.0 (c) VymOs
//
//******************************************************************************

#include "..\inc\MBCb.h"
#include "..\inc\MBCom.h"             // RS232 parametry
#include "..\inc\MBPacket.h"          // zapis dat paketu
#include "..\inc\MBPdu.h"             // kody vyjimek
#include "..\inc\MB.h"                // pomocne operace

// Nejpouzivanejsi kody vyjimek :
// MBEX_ILLEGAL_DATA_ADDRESS  - adresa mimo rozsah zarizeni
// MBEX_ILLEGAL_DATA_VALUE    - hodnota veliciny mimo dovoleny rozsah
// MBEX_SLAVE_DEVICE_FAILURE  - implementacni chyba, nedovoleny kontext

//-----------------------------------------------------------------------------
// Baud
//-----------------------------------------------------------------------------

word MBCGetBaud( void)
// Vrati baudovou rychlost
{
   //>>> nacti konfiguraci
   return( 9600);
} // MBCGetBaud

//-----------------------------------------------------------------------------
// Parity
//-----------------------------------------------------------------------------

byte MBCGetParity( void)
// Vrati paritu (viz MBCom.h)
{
   //>>> nacti konfiguraci
   return( COM_PARITY_NONE);
} // MBCGetParity

//-----------------------------------------------------------------------------
// Timeout
//-----------------------------------------------------------------------------

word MBCGetTimeout( void)
// Vrati timeout v [ms]
{
   //>>> nacti konfiguraci
   return( 60);
} // MBCGetTimeout


//******************************************************************************

//******************************************************************************

#ifndef MB_FIXED_MODE
//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

byte MBCGetMode( void)
// Vrati mod komunikace (viz MBPacket.h)
{
   //>>> nacti konfiguraci
   return( MB_RTU_MODE);
} // MBCGetMode

#endif // MB_FIXED_MODE

#ifdef COM_TX_SPACE
//-----------------------------------------------------------------------------
// Rx/Tx delay
//-----------------------------------------------------------------------------

word MBCGetTxSpace( void)
// Vrati prodlevu mezi Rx a Tx
{
   //>>> nacti konfiguraci
   return( 500);
} // MBCGetTxSpace
#endif // COM_TX_SPACE

//-----------------------------------------------------------------------------
// Address
//-----------------------------------------------------------------------------

byte MBCGetOwnAddress( void)
// Vrati vlastni adresu zarizeni
{
   //>>> nacti konfiguraci
   return( 0x01);
} // MBCGetOwnAddress

//-----------------------------------------------------------------------------
// Slave ID
//-----------------------------------------------------------------------------

word MBCGetSlaveId( void)
// Vrati identifikaci zarizeni
{
   //>>> nacti seriove cislo zarizeni
   return( 0x4567);
} // MBCGetSlaveId

//******************************************************************************

//-----------------------------------------------------------------------------
// Read Discrete
//-----------------------------------------------------------------------------

TYesNo MBCReadDiscrete( word Address, word Count)
// Precte pole binarnich hodnot o delce <Count> bitu
{
byte ByteCount;
byte i;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   ByteCount = (Count + 7) / 8;        // pocet bytu odpovedi
   for( i = 0; i < ByteCount; i++){
      MBWriteByte( 0x5A);              // sestavene bity do bytu <LSB je nizsi adresa>
   }
   return( YES);
} // MBCReadDiscrete

//******************************************************************************


//-----------------------------------------------------------------------------
// Read Coils
//-----------------------------------------------------------------------------

TYesNo MBCReadCoils( word Address, word Count)
// Precte pole binarnich hodnot o delce <Count> bitu
{
byte ByteCount;
byte i;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   ByteCount = (Count + 7) / 8;        // pocet bytu odpovedi
   for( i = 0; i < ByteCount; i++){
      MBWriteByte( 0x5A);              // sestavene bity do bytu <LSB je nizsi adresa>
   }
   return( YES);
} // MBCReadCoils

//-----------------------------------------------------------------------------
// Write Single Coil
//-----------------------------------------------------------------------------

TYesNo MBCWriteSingleCoil( word Address, word Value)
// Zapise binarni hodnotu
{
   //>>> kontrola adresy a hodnoty
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnoty
   if( Value == MB_COIL_OFF){
      // vypnuto
   } else {
      // zapnuto
   }
   return( YES);
} // MBCWriteSingleCoil

//-----------------------------------------------------------------------------
// Write Coils
//-----------------------------------------------------------------------------

TYesNo MBCWriteCoils( word Address, word Count, byte *Data)
// Zapis pole binarnich hodnot o delce <Count> bitu. <Data> je pole bitu
{
byte ByteCount;
byte i;
byte Bits;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   ByteCount = (Count + 7) / 8;        // pocet bytu dat
   for( i = 0; i < ByteCount; i++){
      Bits = Data[ i];                 // sestavene bity do bytu <LSB je nizsi adresa>
   }
   return( YES);
} // MBCWriteCoils

//******************************************************************************

//-----------------------------------------------------------------------------
// Read Input Registers
//-----------------------------------------------------------------------------

TYesNo MBCReadInputRegisters( word Address, word Count)
// Precte pole 16bit hodnot o poctu <Count>
{
byte i;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   for( i = 0; i < Count; i++){
      MBWriteWord( 0x3344);
   }
   return( YES);
} // MBCReadInputRegisters

//******************************************************************************

//-----------------------------------------------------------------------------
// Read Holding Registers
//-----------------------------------------------------------------------------

TYesNo MBCReadRegisters( word Address, word Count)
// Precte pole 16bit hodnot o poctu <Count>
{
byte i;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   for( i = 0; i < Count; i++){
      MBWriteWord( 0x1122);
   }
   return( YES);
} // MBCReadRegisters

//-----------------------------------------------------------------------------
// Write Single Holding Register
//-----------------------------------------------------------------------------

TYesNo MBCWriteSingleRegister( word Address, word Value)
// Zapis 16bit hodnoty <Value>
{
   //>>> kontrola adresy a hodnoty
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnoty
   if( Value > 0x00FF){
      MBException( MBEX_ILLEGAL_DATA_VALUE);
      return( NO);
   }
   return( YES);
} // MBCWriteSingleRegister

//-----------------------------------------------------------------------------
// Write Holding Registers
//-----------------------------------------------------------------------------

TYesNo MBCWriteRegisters( word Address, word Count, word *Data)
// Zapis pole 16bit hodnot o poctu <Count>. <Data> je pole hodnot
{
byte i;
word Value;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   for( i = 0; i < Count; i++){
      Value = MBGetWord( Data[ i]);                // hodnota veliciny
   }
   return( YES);
} // MBCWriteRegisters

//-----------------------------------------------------------------------------
// Mask Write Holding Register
//-----------------------------------------------------------------------------

TYesNo MBCMaskWriteRegister( word Address, word AndMask, word OrMask)
// Zapis registru pomoci masky
{
word Value;

   //>>> kontrola adresy a masky
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnoty
   Value  = 0xFFFF;                    //>>> precti aktualni hodnotu
   Value &= AndMask;
   Value |= OrMask;
   //>>> zapis zpet do aktualni veliciny
   return( YES);
} // MBCMaskWriteRegister

//-----------------------------------------------------------------------------
// Get FIFO length
//-----------------------------------------------------------------------------

word MBCGetFifoLength( void)
// Vrati pocet polozek FIFO
{
   //>>> zjisti delku fronty :
   return( 0xFFFF);
} // MBGetFifoLength

//-----------------------------------------------------------------------------
// Read FIFO queue
//-----------------------------------------------------------------------------

TYesNo MBCReadFifoQueue( word Address, word Count)
// Precte frontu 16bit hodnot v poctu <Count>
{
word i;

   //>>> kontrola adresy a poctu
   if( Address > 0x000F){
      MBException( MBEX_ILLEGAL_DATA_ADDRESS);
      return( NO);
   }
   //>>> zapis hodnot
   for( i = 0; i < Count; i++){
      MBWriteWord( 0x5566);            // zapis polozek fronty
   }
   return( YES);
} // ReadFifoQueue
