//*****************************************************************************
//
//   MBRRegs.h       Modbus process reply - holding registers commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRRegs_H__
   #define __MBRRegs_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBRReadRegisters( word Address, word Count);
// Precte pole 16bit hodnot o poctu <Count>

void MBRWriteSingleRegister( word Address, word Value);
// Zapis 16bit hodnoty <Value>

void MBRWriteRegisters( word Address, word Count, word *Data);
// Zapis pole 16bit hodnot o poctu <Count>. <Data> je pole hodnot

void MBRMaskWriteRegister( word Address, word AndMask, word OrMask);
// Zapis registru pomoci masky

void MBRReadWriteRegisters( word ReadAddress, word ReadCount, word WriteAddress, word WriteCount, word *Data);
// Zapis pole 16bit hodnot o delce <WriteCount>, <Data> je pole hodnot.
// Cteni pole 16bit hodnot o delce <ReadCount>

#endif
