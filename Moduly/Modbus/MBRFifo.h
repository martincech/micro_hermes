//*****************************************************************************
//
//   MBRFifo.h       Modbus process reply - FIFO registers commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRFifo_H__
   #define __MBRFifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBRReadFifoQueue( word Address);
// Precte frontu 16bit hodnot

#endif
