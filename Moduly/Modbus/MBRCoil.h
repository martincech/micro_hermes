//*****************************************************************************
//
//   MBRCoil.h       Modbus process reply - coils commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRCoil_H__
   #define __MBRCoil_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBRReadCoils( word Address, word Count);
// Precte pole binarnich hodnot o delce <Count> bitu

void MBRWriteSingleCoil( word Address, word Value);
// Zapise binarni hodnotu

void MBRWriteCoils( word Address, word Count, byte *Data);
// Zapis pole binarnich hodnot o delce <Count> bitu. <Data> je pole bitu


#endif
