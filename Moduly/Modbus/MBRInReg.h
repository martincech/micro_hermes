//*****************************************************************************
//
//   MBRInReg.h      Modbus process reply - input registers commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRInReg_H__
   #define __MBRInReg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBRReadInputRegisters( word Address, word Count);
// Precte pole 16bit hodnot o poctu <Count>

#endif
