//*****************************************************************************
//
//   MBRRegs.c       Modbus process reply - holding registers commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "MBRRegs.h"
#include "..\inc\MBPacket.h"          // zapis dat paketu
#include "..\inc\MBCb.h"              // vykonne callbacky
#include "..\inc\MB.h"                // pomocne operace
#include "..\inc\MBPdu.h"             // MBBitsToByte

//-----------------------------------------------------------------------------
// Read registers
//-----------------------------------------------------------------------------

void MBRReadRegisters( word Address, word Count)
// Precte pole 16bit hodnot o poctu <Count>
{
   // zahlavi odpovedi :
   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteByte( 2 * Count);
   // vykonny callback :
   if( !MBCReadRegisters( Address, Count)){
      return;                          // exception
   }
   MBWriteCrc();                       // dokonceni paketu
} // MBRReadRegisters

//-----------------------------------------------------------------------------
// Write single register
//-----------------------------------------------------------------------------

void MBRWriteSingleRegister( word Address, word Value)
// Zapis 16bit hodnoty <Value>
{
   // vykonny callback :
   if( !MBCWriteSingleRegister( Address, Value)){
      return;                          // exception
   }
   // odpoved :
   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteWord( Address);
   MBWriteWord( Value);
   MBWriteCrc();
} // MBRWriteSingleRegister

//-----------------------------------------------------------------------------
// Write registers
//-----------------------------------------------------------------------------

void MBRWriteRegisters( word Address, word Count, word *Data)
// Zapis pole 16bit hodnot o poctu <Count>. <Data> je pole hodnot
{
   // vykonny callback :
   if( !MBCWriteRegisters( Address, Count, Data)){
      return;                          // exception
   }
   // odpoved :
   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteWord( Address);
   MBWriteWord( Count);
   MBWriteCrc();                       // dokonceni paketu
} // MBRWriteRegisters

//-----------------------------------------------------------------------------
// Write mask
//-----------------------------------------------------------------------------

void MBRMaskWriteRegister( word Address, word AndMask, word OrMask)
// Zapis registru pomoci masky
{
   // vykonny callback :
   if( !MBCMaskWriteRegister( Address, AndMask, OrMask)){
      return;                          // exception
   }
   // odpoved :
   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteWord( Address);
   MBWriteWord( AndMask);
   MBWriteWord( OrMask);
   MBWriteCrc();
} // MBRMaskWriteRegister

//-----------------------------------------------------------------------------
// Read / Write registers
//-----------------------------------------------------------------------------

void MBRReadWriteRegisters( word ReadAddress, word ReadCount, word WriteAddress, word WriteCount, word *Data)
// Zapis pole 16bit hodnot o delce <WriteCount>, <Data> je pole hodnot.
// Cteni pole 16bit hodnot o delce <ReadCount>
{
   // vykonny callback :
   if( !MBCWriteRegisters( WriteAddress, WriteCount, Data)){
      return;                          // exception
   }
   // odpoved :
   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteByte( 2 * ReadCount);
   // vykonny callback :
   if( !MBCReadRegisters( ReadAddress, ReadCount)){
      return;                          // exception
   }
   MBWriteCrc();                       // dokonceni paketu
} // MBRReadWriteRegisters

