//*****************************************************************************
//
//    AT25512.c51  AT25512/AT25256 EEPROM via HW SPI
//    Version 1.0
//
//*****************************************************************************

// Funkcne shodne s modulem AT25256, ktery ale pouziva svoje SPI a nechavam ho kvuli TMC,
// aby se tam nemuselo nic menit.

#include "..\inc\At25512.h"

//-----------------------------------------------------------------------------
// Definice
//-----------------------------------------------------------------------------

// Instrukce AT25256 :
#define EEP_WREN        0x06            // Set Write Enable Latch (Enable Write)
#define EEP_WRDI        0x04            // Reset Write Enable/Reset Flag Bit
#define EEP_RDSR        0x05            // Read Status Register
#define EEP_WRSR        0x01            // Write Status Register
#define EEP_READ        0x03            // Read data
#define EEP_WRITE       0x02            // Write data

// Status register format AT25256 :
#define EEP_MASK_RDY    0x01            // -READY, je v 1 probiha-li zapis
#define EEP_MASK_WEN    0x02            // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BP0    0x04            // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BP1    0x08            // ochranny bit 1 (1/2 kapacity)
#define EEP_MASK_WPEN   0x80            // ovladani Write Protect /WP pinu

//-----------------------------------------------------------------------------
// Lokalni funkce
//-----------------------------------------------------------------------------

static void WrenCmd( void);
// Posle instrukci WREN

static byte RdsrCmd( void);
// Precte a vrati status registr

static void WrsrCmd( byte value);
// Zapise do status registru

static void WaitForReady( void);
// Cekani na pripravenost

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void EepInit()
// Nastavi klidove hodnoty na sbernici, inicializuje pamet
{
  SpiInit();                     // vychozi stav SPI
  SpiRelease();                  // deselect
  // zapis do stavoveho registru :
  WaitForReady();
  WrenCmd();                     // Set write enable latch
  WrsrCmd( 0);                   // Zrusit ochranu pameti
} // EepInit

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

#ifdef EEP_READ_BYTE

byte EepReadByte( word address)
// Precte byte z EEPROM <address>
{
byte value;

   EepBlockReadStart( address);   // zacatek bloku
   value = SpiReadByte();         // jeden datovy byte
   SpiRelease();                  // deselect
   return( value);
} // EepReadByte

#endif // EEP_READ_BYTE

//-----------------------------------------------------------------------------
// Strankove cteni z pameti
//-----------------------------------------------------------------------------

void EepBlockReadStart( word address)
// Zahaji blokove cteni z EEPROM <address>
{
   WaitForReady();                // pripravenost
   SpiAttach();                   // SPI mode 0
   SpiWriteByte( EEP_READ);      // READ instruction
   SpiWriteByte( address >> 8);  // high address
   SpiWriteByte( address);       // low address
} // EepBlockReadStart

#ifdef EEP_WRITE_BYTE
//-----------------------------------------------------------------------------
// Zapis do pameti
//-----------------------------------------------------------------------------

TYesNo EepWriteByte( word address, byte value)
// Zapise byte <value> na <address> v EEPROM
{
   if( !EepPageWriteStart( address)){
      return( NO);
   }
   SpiWriteByte( value);         // data
   SpiRelease();                  // deselect
   return( YES);                  // necekame na dokonceni zapisu
} // EepWriteByte

#endif // EEP_WRITE_BYTE

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

TYesNo EepPageWriteStart( word address)
// Zahaji zapis stranky od <address> v EEPROM
{
   if( !EepAccuOk()){
      return( NO);
   }
   WaitForReady();                // cekani na pripravenost
   WrenCmd();                     // Set write enable latch
   SpiAttach();                   // SPI mode 0
   SpiWriteByte( EEP_WRITE);     // WRITE instruction
   SpiWriteByte( address >> 8);  // high address
   SpiWriteByte( address);       // low address
   return( YES);
} // EepPageWriteStart

//-----------------------------------------------------------------------------
// Ulozeni dat
//-----------------------------------------------------------------------------

#ifdef EEP_SAVE_DATA

TYesNo EepSaveData(word Address, byte __xdata__ *Data, byte Size) {
  // Ulozi na adresu <Address> data v <Data> o velikosti <Size> bajtu
  TYesNo NewPage = YES;                 // Na zacatku cyklu chci hned odstartovat blokovy zapis

  while (Size-- > 0) {
    if (NewPage) {
      // Zacatek nove stranky
      NewPage = NO;
      if (!EepPageWriteStart(Address)) {
        return NO;
      }
    }
    EepPageWriteData(*Data);
    Data++;
    Address++;
    if (Address % EEP_PAGE_SIZE == 0) {
      // Nova adresa uz lezi v nove strance, provedu zapis stranky
      EepPageWritePerform();            // Zapisu predchozi stranku
      NewPage = YES;                    // V pristim cyklu provedu start zapisu nove stranky
    }
  }

  if (!NewPage) {
    EepPageWritePerform();              // Zapisu zbytek posledni stranky
  }

  return YES;
} // EepSaveData

#endif // EEP_SAVE_DATA

//-----------------------------------------------------------------------------
// Nacteni dat
//-----------------------------------------------------------------------------

#ifdef EEP_READ_DATA

void EepReadData(word Address, byte __xdata__ *Data, byte Size) {
  // Nacte data od adresy <Address> o velikosti <Size> bajtu do promenne <Data>
  EepBlockReadStart(Address);
  while (Size-- > 0) {
    *Data = EepBlockReadData();
    Data++;
  }
  EepBlockReadStop();
} // EepReadData

#endif // EEP_READ_DATA

//-----------------------------------------------------------------------------
// Vyplneni pameti hodnotou
//-----------------------------------------------------------------------------

#ifdef EEP_FILL

TYesNo EepFill(word StartAddress, word EndAddress, byte Value) {
  // Vyplni oblast <StartAddress> az <EndAddress> vcetne hodnotou <Value>
  TYesNo NewPage = YES;                 // Na zacatku cyklu chci hned odstartovat blokovy zapis

  while (StartAddress <= EndAddress) {
    if (NewPage) {
      // Zacatek nove stranky
      NewPage = NO;
      if (!EepPageWriteStart(StartAddress)) {
        return NO;
      }
    }
    EepPageWriteData(Value);
    StartAddress++;
    if (StartAddress % EEP_PAGE_SIZE == 0) {
      // Nova adresa uz lezi v nove strance, provedu zapis stranky
      EepPageWritePerform();            // Zapisu predchozi stranku
      NewPage = YES;                    // V pristim cyklu provedu start zapisu nove stranky
      WatchDog();                       // Muze to trvat dlouho (prepisovat muze klidne celou pamet, coz u AT25256 a HW SPI trva cca 2sec)
    }
  }

  if (!NewPage) {
    EepPageWritePerform();              // Zapisu zbytek posledni stranky
  }

  return YES;
} // EepFill

#endif // EEP_FILL

//-----------------------------------------------------------------------------
// Instrukce WREN
//-----------------------------------------------------------------------------

static void WrenCmd()
// Posle instrukci WREN
{
   SpiAttach();                   // SPI mode 0
   SpiWriteByte( EEP_WREN);
   SpiRelease();                  // deselect
} // WrenCmd

//-----------------------------------------------------------------------------
// Instrukce RDSR
//-----------------------------------------------------------------------------

static byte RdsrCmd()
// Precte a vrati status registr
{
byte value;

   SpiAttach();                   // SPI mode 0
   SpiWriteByte( EEP_RDSR);
   value  = SpiReadByte();
   SpiRelease();                  // deselect
   return( value);
}  // RdsrCmd

//-----------------------------------------------------------------------------
// Instrukce WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte value)
// Zapise do status registru
{
   SpiAttach();                   // SPI mode 0
   SpiWriteByte( EEP_WRSR);
   SpiWriteByte( value);
   SpiRelease();                  // deselect
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Cekani na dokonceni zapisu
//-----------------------------------------------------------------------------

static void WaitForReady()
// Cekani na pripravenost
{
   while( RdsrCmd() & EEP_MASK_RDY);
} // WaitForReady
