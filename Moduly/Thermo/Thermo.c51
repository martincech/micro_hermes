//*****************************************************************************
//
//    Thermo.c - Thermometer services module
//    Version 1.2  (c) VymOs
//
//*****************************************************************************

/*

Verze 1.2
  - 23.10.2006: Pridana moznost periodicke inicializace cidel (vypne a zapne napajeni)

 */


#include "..\inc\Thermo.h"

#define THERMO_INVALID AUTO_TEMP_ERROR

// Periodicka inicializace cidel
#ifdef THERMO_PERIODIC_INIT
  typedef enum {
    MODE_READING,         // Probiha nacitani teplot
    MODE_POWER_OFF        // Cidla maji vypnute napajeni
  } TMode;
  static TMode __xdata__ Mode;            // Aktualni rezim
  static byte __xdata__  ModeCounter;     // Pocitadlo pro odpocet casu
#endif //THERMO_PERIODIC_INIT

//-----------------------------------------------------------------------------
// Inicializace teplomeru
//-----------------------------------------------------------------------------

void ThermoInit( void)
// Inicializuje skupinu teplomeru
{
   TempInit();                         // inicializace sbernice
   TempStartConversion();              // prvni odecet
   // Inicializace periodicke inicializace cidel
#ifdef THERMO_PERIODIC_INIT
   Mode        = MODE_READING;
   ModeCounter = 0;
#endif //THERMO_PERIODIC_INIT
} // ThermoInit

//-----------------------------------------------------------------------------
// Mereni
//-----------------------------------------------------------------------------

void ThermoMeasure( void)
// Zmeri skupinu teplot. Volat po 1s
{
byte i;      // adresa cidla
byte j;      // pocet pokusu
int  temp;

   // Obsluha periodicke inicializace cidel
#ifdef THERMO_PERIODIC_INIT
   ModeCounter++;       // Prictu pocitadlo, pouziva se u obou rezimu
   switch (Mode) {
     case MODE_READING:
       // Teploty se normalne nacitaji
       if (ModeCounter > THERMO_PERIODIC_INIT_ON) {
         // Merim uz predepsanou dobu, vypnu cidla
         TempPowerOff();                // Vypnu napajeni cidel
         Mode        = MODE_POWER_OFF;  // Prejdu do vypnuteho stavu
         ModeCounter = 0;               // Nuluju pocitadlo
         return;                        // V mereni dal nepokracuju (cidla jsou vypnuta)
       }
       // Merim dal...
       break;

     default: // MODE_POWER_OFF
       // Teplomery maji vypnute napajeni
       if (ModeCounter > THERMO_PERIODIC_INIT_OFF) {
         // Cidla jsou vypnuta uz predepsanou dobu, opet zapnu mereni
         ThermoInit();                  // Rozjedu mereni, zarven nastavi rezim i nuluje pocitadlo
         return;                        // V mereni v tomto kroce nepokracuju, volani ThermoInit() zahajilo konverzi, ktera stejne trva 750ms.
       }
       // Cekam dal s vypnutymi cidly...
       return;  // S vypnutymi cidly nemerim
   }//switch
#endif //THERMO_PERIODIC_INIT

   // Precteme hodnoty vsech kanalu :
   for( i = MWI_CH0; i < _THERMO_LAST; i++){
      for( j = THERMO_TRIALS; j > 0; j--){
         // Opakovani pokusu o cteni :
         temp = TempRead( i);
         if( temp != TEMP_INVALID){
            break;                     // uspesne precteno
         }
      }
      AlogTemperature( i, temp);       // zapsat mereni
   }
   TempStartConversion();
} // ThermoMeasure


//-----------------------------------------------------------------------------
// Prevod a zaokrouhleni teploty z interniho formatu na cele stupne
//-----------------------------------------------------------------------------

#ifdef THERMO_CONVERT_1C

char TempConvert1C(int temp)
// Prevede teplotu na cele stupne se zaokrouhenim
{
   if( temp == THERMO_INVALID){
      // chybne mereni teploty
      return(TempGet1C(THERMO_INVALID)); // jen cele stupne
   }
   // zaokrouhleni :
   // 28.5.2003: Zaokrouhleni bylo spatne, musim samozrejme pricitat pul stupne, ne 0.25
   if( temp >= 0){
      // kladna teplota
      temp += TempMk001C( 0, 50);             // zvetsi o 0.5 C
      return( TempGet1C( temp));              // jen cele stupne
   } else {
      // zaporna teplota
      temp -= TempMk001C( 0, 50);             // zmensi o -0.5 C
      // 15.9.2003: V return() musi byt i znamenko, protoze TempGet1C() vraci vzdy jen absolutni hodnotu
      return( -TempGet1C( temp));              // jen cele stupne
   }
} // TempConvert1C

#endif

//-----------------------------------------------------------------------------
// Prevod a zaokrouhleni teploty z interniho formatu na desetiny stupne
//-----------------------------------------------------------------------------

#ifdef THERMO_CONVERT_01C

int TempConvert01C(int temp) {
  // Prevede teplotu na desetiny stupne se zaokrouhenim
   if( temp == THERMO_INVALID){
      // chybne mereni teploty
      return(10 * TempGet1C(THERMO_INVALID)); // jen cele stupne
   }
  if( temp >= 0){
    // kladna teplota
    temp += TempMk001C( 0, 5);              // zvetsi o 0.05 C
    return( 10 * TempGet1C( temp) + TempGet01C(temp));
  } else {
    // zaporna teplota
    temp -= TempMk001C( 0, 5);              // zmensi o -0.05 C
    // 15.9.2003: V return() musi byt i znamenko, protoze TempGet1C() vraci vzdy jen absolutni hodnotu
    return( -10 * TempGet1C( temp) - TempGet01C(temp));
  }
} // TempConvert01C

#endif
