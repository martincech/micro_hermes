//*****************************************************************************
//
//    Ds18x20.c  -  1-Wire temperature sensor services
//    Version 1.1, (c) Vymos
//
//*****************************************************************************

/*

 Verze 1.1:
   - 16.10.2006: Pridan strong pull-up, 2 vodicove provedeni bylo dosud spatne. Po zahajeni konverze je treba pripnout sbernici na napajeni po
                 dobu 750ms (pro 12bit prevod). Delam to tak, ze po zahajeni konverze sepnu pull-up a vypnu ho az pred ctenim teplot (1sec perioda).

 */

#define __DEBUG__        // povoleno ladeni

#include "..\inc\Ds18x20.h"

// Prikazy pro DS18x20 :

#define TEMP_CMD_SEARCH_ROM    MWI_CMD_SEARCH_ROM   // vyhledani a identifikace zarizeni
#define TEMP_CMD_READ_ROM      MWI_CMD_READ_ROM     // cteni identifikace (pro 1 zarizeni)
#define TEMP_CMD_MATCH_ROM     MWI_CMD_MATCH_ROM    // vyber zarizeni podle identifikace
#define TEMP_CMD_SKIP_ROM      MWI_CMD_SKIP_ROM     // vyrazeni identifikace (pro 1 zarizeni)
#define TEMP_CMD_ALARM_SEARCH  MWI_CMD_ALARM_SEARCH // vyhledani a identifikace zarizeni s alarmem

#define TEMP_CMD_CONVERT_T        0x44    // zahajeni konverze
#define TEMP_CMD_WRITE_SCRATCHPAD 0x4E    // zapis do bufferu
#define TEMP_CMD_READ_SCRATCHPAD  0xBE    // cteni bufferu
#define TEMP_CMD_COPY_SCRATCHPAD  0x48    // zapis bufferu do EEPROM
#define TEMP_CMD_RECAL_EE         0xB8    // cteni z EEPROM do scratchpad
#define TEMP_CMD_READ_POWER       0xB4    // identifikace napajeni

// Adresace pro DS18x20 :

#define TEMP_ADDR_LSB             0       // dolni hodnota teploty
#define TEMP_ADDR_MSB             1       // horni hodnota teploty
#define TEMP_ADDR_TH              2       // horni alarm
#define TEMP_ADDR_TL              3       // dolni alarm

#ifdef TEMP_DS18B20
#define TEMP_ADDR_CONFIG          4       // jen DS18B20
#endif

#ifdef TEMP_DS18S20
#define TEMP_ADDR_COUNT_REMAIN    6       // zbytek po konverzi
#define TEMP_ADDR_COUNT_PER_C     7       // impulsy pro 1C
#endif

#define TEMP_ADDR_CRC             8       // zabezpeceni
#define TEMP_SCRATCHPAD_LENGTH    9       // celkova delka bufferu

// Konfiguracni registr DS18B20 :
#ifdef TEMP_18B20
#define TEMP_MASK_9BIT            0x00    // rozliseni 9 bitu
#define TEMP_MASK_10BIT           0x20    // rozliseni 10 bitu
#define TEMP_MASK_11BIT           0x40    // rozliseni 11 bitu
#define TEMP_MASK_12BIT           0x60    // rozliseni 12 bitu
#endif

// Casovani operaci (ms) :

#define TEMP_DELAY_EEPROM         10      // zapis scratchpad->EEPROM

#ifdef TEMP_DS18B20
#define TEMP_DELAY_9BIT           74      // rozliseni 9 bitu
#define TEMP_DELAY_10BIT          188     // rozliseni 10 bitu
#define TEMP_DELAY_11BIT          375     // rozliseni 11 bitu
#define TEMP_DELAY_12BIT          750     // rozliseni 12 bitu
#endif

#ifdef TEMP_DS18S20
#define TEMP_DELAY_CONVERSION     750     // rozliseni 9 bitu
#endif

// Implementacni konstanty :

#ifdef TEMP_DS18B20
#define TEMP_MASK_CONVERSION   TEMP_MASK_12BIT    // rozliseni prevodniku
#define TEMP_DELAY_CONVERSION  TEMP_DELAY_12BIT   // zpozdeni pri prevodu
#endif

// Typ pripojeni ke sbernici :

#ifdef MwiDATA
   // sdilena sbernice
   #define TempAttach()     MwiAttach()
   #define TempRelease()    MwiRelease()
#else
   // individualni pripojeni
   #define TempAttach()
   #define TempRelease()
#endif

//-----------------------------------------------------------------------------
// Start konverze
//-----------------------------------------------------------------------------

void TempStartConversion( void)
// Vysle prikaz zacatku konverze
{
   TempAttach();
   MwiResetAll();
   MwiWriteByte( TEMP_CMD_SKIP_ROM);
   MwiWriteByte( TEMP_CMD_CONVERT_T);
   TempRelease();
#ifdef TEMP_STRONG_PULLUP
   TempUP = TEMP_PULLUP_ON;     // Nahodim pull-up - je treba do 10us po vyslani prikazu na zahajeni konverze
#endif // TEMP_STRONG_PULLUP
} // TempStartConversion

//-----------------------------------------------------------------------------
// Cteni teploty
//-----------------------------------------------------------------------------

int TempRead( byte channel)
// Vrati prectenou teplotu, v hornim bytu jsou stupne C,
// v dolnim pokracuji desetiny (MSB = 0.5, 0.25, 0.125...LSB)
{
int value;
#ifdef TEMP_SECURE_READ
   byte i;
#endif

#ifdef TEMP_STRONG_PULLUP
   TempUP = !TEMP_PULLUP_ON;    // Shodim pull-up
   _nop_();                     // Radeji chvilicku pockam
#endif // TEMP_STRONG_PULLUP

   TempAttach();
   if( !MwiReset( channel)){
      TempRelease();
      return( TEMP_INVALID);           // neni presence, vadny senzor
   }
   MwiWriteByte( TEMP_CMD_SKIP_ROM);
   MwiWriteByte( TEMP_CMD_READ_SCRATCHPAD);
   value  = (int)MwiReadByte( channel);
   value |= (int)MwiReadByte( channel) << 8;
   // Pozor, pokud se necte vsechno, musi nasledovat reset
#ifdef TEMP_SECURE_READ
   MwiResetCrc();
   MwiCalcCrc( (byte)value);
   MwiCalcCrc( ((word)value >> 8) & 0xFF);
   // docteni zbytku dat, crc je posledni :
   for( i = 0; i < TEMP_SCRATCHPAD_LENGTH - 2; i++){
      MwiCalcCrc( MwiReadByte( channel));
   }
   // mel by byt 0 :
   if( MwiGetCrc() != 0){
      TempRelease();
      return( TEMP_INVALID);           // chybne cteni
   }
#endif
   TempRelease();
   value <<= TEMP_SHIFT;               // puvodni LSB 0.5 C / 0.0625 C
   if( value == (TEMP_INITIAL << 8)){
      return( TEMP_INVALID);           // neinicializovany senzor
   }
   return( value);
} // TempRead

//-----------------------------------------------------------------------------
// Vypnuti napajeni cidel
//-----------------------------------------------------------------------------

void TempPowerOff() {
  // Vypne napajeni cidlum (opetovne zapnuti pomoci TempStartConversion())
#ifdef TEMP_STRONG_PULLUP
  TempUP = !TEMP_PULLUP_ON;     // Shodim pull-up
#endif // TEMP_STRONG_PULLUP
  MwiPowerOff();                // Shodim napeti na sbernici
}
