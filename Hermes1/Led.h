//*****************************************************************************
//
//    Led.c - Ovladani LED diod
//    Version 1.0
//
//*****************************************************************************


#ifndef __Led_H__
   #define __Led_H__

#include "Hardware.h"     // pro podminenou kompilaci


// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void LedInit(void);
// Inicializuje

void LedOff(void);
// Zhasne obe LED

void LedTrigger(TYesNo ShowAlarm);
// Obsluha casovace, volat kazdych TIMER0_PERIOD ms


#endif // __Led_H__
