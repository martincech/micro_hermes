//*****************************************************************************
//
//    Main.c51 -  Hermes1 main
//    Verze 1.00  P.Veit
//
//*****************************************************************************


/*
Verze 1.00:
  - 20.1.2006: Prvni betaverze nabastlena na desky Bat1 (prijimac) a Bat2 (vysilac).

Verze 2.00:
  - 6.4.2006: Zobrazeni predelano na displej UG3264gm

Verze 2.01:
  - 4.5.2009: Prijimac nyni vyhlasi alarm pokud neni signal (dosud se jen zobrazila hlaska
              na displeji). U spatneho signalu lze navolit zvlast ton i hlasitost piskani.
              U vysilace neni zadna zmena, jen jsem posunul cislo verze.

Verze 2.02:
  - 12.10.2009: Prelozeno do nemciny pro Cobb Germany (nainstalovane do naseho auta).

Verze 3.00:
  - 3.10.2016: Prechod na novy displej. Ovladac delal jeste Martin Dusek, ja jsem to pozdeji zkompiloval.

 */




#include "Hardware.h"
#include "Hermes1.h"
#include "Podsvit.h"           // Roznuti dospleje po stisku klavesy
#include "Menu.h"              // Uzivatelske rozhrani
#include "Cfg.h"               // Konfigurace
#include "KbdCtl.h"            // Obsluha klavesnice
#include "Temp.h"              // Cteni teplot
#include "Alarm.h"             // Hlaseni poruchy
#include "Beep.h"              // Repro v jednotce
#include "Transmit.h"          // Vysilani dat

#include "..\inc\System.h"     // "operacni system"
#include "..\inc\Kbd.h"        // klavesnice
#include "..\inc\Pca.h"        // zvuky pomoci PCA
#include "..\inc\bcd.h"        // BCD aritmetika


// Odpocet necinnosti :
#define USER_EVENT_TIMEOUT 240    // Uzivatel neobsluhuje dele nez ... s - nastavil jsem to schvalne na 4 minuty, tj. co nejdelsi

#define NastavPocitadloSekundy() PocitadloSekundy = 1000/TIMER0_PERIOD   // Novy cyklus odpocitavani 1 sekundy

// Obsluha blikani :
volatile byte BlinkCounter = BLINK_ON;   // Pocitadlo, ktere urcuje periodu blikani
volatile bit  BlinkShow    = YES;        // Zda se ma prave zobrazit nebo skryt
volatile bit  BlinkChange  = NO;         // Flag, ze se zmenil stav BlinkShow a ma se tedy roznout nebo zhasnout => ma se zmenit stav

// Promenne pro timer 1 sekundy
volatile byte        PocitadloSekundy=0;   // Odpocitavani 1s
volatile bit         UbehlaSekunda=0;      // Priznak odpocitani 1s
static volatile bit  PocitatNecinnost=1;   // Zda mam pocitat necinnost klavesnice nebo ne
static volatile byte _Timeout=0;           // Pocitani necinnosti

// Rezim cinnosti
typedef enum {
  EXECUTE_MEASURE,      // Normalni mereni
  EXECUTE_LANGUAGE,     // Nastaveni jazyka
  EXECUTE_COUNT
} TExecuteMode;
TExecuteMode __xdata__ ExecuteMode;



// Globalni promenne
TUn UN;

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay( word ms)
// Zpozdeni v milisekundach
{
byte m;                                                                     // m = R5, n = R6+R7

  while(ms > 0) {              // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*

#if (FXTAL == 18432000L)

    // Xtal 18.432 MHz v X2 modu, T = 0.325521 us
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    // Pro X2 mod musim 2x tolik!!
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    --ms;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*

#elif (FXTAL == 4912500L)

    // Xtal 4.9125 MHz v X2 modu, T = 1.221374 us
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 153; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    --ms;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*

#else

  #error "SysDelay: not defined for FXTAL"

#endif // FXTAL

  }//while                                                                       // SJMP DELAY *2T*
} // SysDelay

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

byte SysYield()
// Prepnuti kontextu na operacni system. Vraci udalost
{
byte Key;

  WatchDog();                  // Zahybu watchdogem

  // Casovac periodicke cinnosti :
  if (ExecuteMode == EXECUTE_MEASURE) {       // Probiha normalni cinnost (tj. nenastavuje jazyk ani nekalibruje)
    TransmitCheckTransmitter();  // Hlidani vypinani napajeni vysilace po odeslani paketu
    if (UbehlaSekunda) {
      // Ubehla 1 sekunda
      UbehlaSekunda = 0;         // Shodim flag
      NastavPocitadloSekundy();  // Novy cyklus odpocitavani 1 sekundy
      if (PocitatNecinnost) {
        _Timeout++;              // Pocitam necinnost
      } else {
        _Timeout = 0;            // Nepocitam necinnost, stale nuluju
      }//else
      //--------- periodicka cinnost 1s :
      AlarmClear();              // Nulovani poruch na zacatku kazdeho mericiho cyklu
      PodsvitExecute();          // Ovladaniu podsvitu
      TemperatureRead();         // Nacteni teploty
      TransmitExecute();         // Obsluha vysilani dat - vzdy az na konci sekundoveho tiku, aby se vysilalo v case, kdy ze nectou teplomery!
      Display |= DISPLAY_1SEC;   // Zobrazeni po 1sec
    }//if
  }//if

  // Casovac blikani
  if (BlinkChange) {
    // Uplynul cas blikani - bud mam skryt nebo zobrazit
    BlinkChange = 0;          // Shodim flag
    if (BlinkShow) {
      return K_BLINK_ON;
    } else {
      return K_BLINK_OFF;
    }//else
  }//if

  // Testovani necinnosti
  if (_Timeout > USER_EVENT_TIMEOUT) {
    _Timeout = 0;              // Zahajime dalsi cekani
    return K_TIMEOUT;
  }//if

  // cteni klavesy :
  Key = KbdGet();
  if (Key != K_IDLE) {
    _Timeout = 0;     // Konec necinnosti
    PodsvitAutoOn();  // Rozne automaticky podsvit
    return Key;
  }//if

  // Kdyz mam co zobrazit, poslu prikaz k prekresleni
  if (Display) {
    return K_REDRAW;
  }

  // Pokud dosel az sem, neprobehla zadna udalost
  return K_IDLE;
} // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent()
// Cekani na udalost
{
byte Key;

  while(1) {
    Key = SysYield();
    if (Key != K_IDLE) {
      return Key;  // Neprazdna udalost
    }//if
  }//while
} // SysWaitEvent
/*
//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysFlushTimeout( void)
// Nuluje timeout klavesnice
{
   _Timeout = 0;
   // nastavit kontext v modulu Kbd, aby nezustal vysilat treba K_REPEAT
} // SysFlushTimeout
*/
//-----------------------------------------------------------------------------
// Zakaz timeoutu viz System.h
//-----------------------------------------------------------------------------

/*void SysDisableTimeout( void)
// Zakaze timeout klavesnice
{
   _Timeout = 0;
   PocitatNecinnost = NO;
} // SysDisableTimeout*/

//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

/*void SysEnableTimeout( void)
// Povoli timeout klavesnice
{
   _Timeout = 0;
   PocitatNecinnost = YES;
} // SysEnableTimeout*/

//-----------------------------------------------------------------------------
// Nastaveni timeoutu viz System.h
//-----------------------------------------------------------------------------

/*void SysSetTimeout( void)
// Nastavi timeout klavesnice
{
   _Timeout = USER_EVENT_TIMEOUT + 1;
} // SysSetTimeout*/

// ---------------------------------------------------------------------------------------------
// Preruseni casovace
// ---------------------------------------------------------------------------------------------

static void Timer0Handler() interrupt INT_TIMER0 {
  // Prerusovaci rutina casovace 0

  SetTimer0(TIMER0_PERIOD);                   // S touto periodou

  // casovac blikani :
  BlinkCounter--;
  if (BlinkCounter == 0) {
     // docitali jsme, nastav na novou hodnotu
     BlinkChange = 1;                        // nastav priznak
     if (BlinkShow) {
       BlinkCounter = BLINK_OFF;          // bylo roznuto,  perioda pro zhasni
     } else {
       BlinkCounter = BLINK_ON;           // bylo zhasnuto, perioda pro rozsvit
     }//else
     BlinkShow = !BlinkShow;      // novy smer citani
  }//if
  // vykonne funkce :
  CheckTrigger( PocitadloSekundy, UbehlaSekunda = 1);   // casovac 1 s
  PcaTrigger();                                         // handler asynchronniho pipani
  KbdTrigger();                                         // casovac klavesnice
  AlarmTrigger();                                       // Casovac alarmu (LED)
} // Timer0Handler

// ---------------------------------------------------------------------------------------------
// Testovani pinu
// ---------------------------------------------------------------------------------------------

#define HybejPinem(Pin) \
  EA = 0;               \
  while (!EA) {         \
    Pin = 1;            \
    Delay(2000);         \
    Pin = 0;            \
    Delay(500);         \
  }                     \

#define HybejPinemRychle(Pin) \
  EA = 0;               \
  while (!EA) {         \
    Pin = 1;            \
    WatchDog();         \
    Pin = 0;            \
  }                     \

#define HybejPinemRychleAsym(Pin) \
  EA = 0;               \
  while (!EA) {         \
    Pin = 1;            \
    _nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();    \
    WatchDog();         \
    Pin = 0;            \
  }                     \

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

void main()
{
//  ZapniPodsvit();             // zapni podsvit

  TimingSetup();              // Nastaveni X2 modu
  EnableXRAM();               // Povoleni pristupu k vnitrni XRAM
  WDTPRG = WDTPRG_2090;       // start Watch Dogu
  WatchDog();

  //HybejPinem(MwiDQO);
//  HybejPinemRychle(MwiDQO);
//  HybejPinemRychle(HermesD0);
//  HybejPinemRychleAsym(MwiDQO);

  NastavPocitadloSekundy();   // Nastartuju odpocitavani sekundy

  // Inicializace vsech modulu
  Delay(300);                 // Pockam, dokud nedojde k vzrustu napajeni - nemam k dispozici lowbat

  AlarmInit();                // Alarm musim inicializovat jeste pred spustenim preruseni timeru (jinak nahodne blikaji LED)

  EnableInts();               // Povoleni preruseni
  Timer0Run(TIMER0_PERIOD);   // Spust casovac 0

  BeepStartup();              // uvodni pipnuti

  CfgLoad();                  // Nacteni konfigurace z interni EEPROM

  KbdInit();
  MenuInit();                 // inicializace zobrazeni (az za CfgLoad)
  PodsvitInit();              // Inicializace podsvitu
  TemperatureInit();          // Inicializace teplomeru
  TransmitInit();             // Vysilani dat

  // Po resetu otestuju, zda nechce zmenit jazyk
  switch (KbdPowerUpKey()) {
    case K_ENTER:
      // Nastaveni jazyka
      KbdBeepKey();                     // Indikace, ze muze klavesu pustit
      KbdPowerUpRelease();              // Cekam na pusteni
      ExecuteMode = EXECUTE_LANGUAGE;   // Nastavim flag, ze se nastavuje jazyk a vaha nema provadet zadnou cinnost
      MenuLanguage();
      while (1);                        // Pockam na reset

    case K_ESC:
      // Diagnosticky rezim
      KbdBeepKey();                     // Indikace, ze muze klavesu pustit
      ShowDiagnostic = YES;             // Nastavim flag, ze se nastavuje jazyk a vaha nema provadet zadnou cinnost
      // Pokracuju dal na default:

    default:
      // Normalni cinnost
      ExecuteMode = EXECUTE_MEASURE;    // Normalni rezim
  }//switch

  SysDelay(3000);                       // Cekani pri zobrazeni loga

  // Hlavni smycka
  while(1) {
    switch(SysWaitEvent()) {
      case K_ENTER: {
        // Vratim default font i mod fontu
        KbdBeepKey();
        MenuExecute();   // Rozjedu menu
        ZerKlavesu();
        MenuRedraw(DISPLAY_ALL);
        break;
      }
      case K_RIGHT: {
        break;
      }
      case K_LEFT: {
        break;
      }
      case K_UP: {
        break;
      }
      case K_DOWN: {
        break;
      }
      case K_ESC: {
        KbdBeepKey();
        AlarmChange();                  // Zapnu/vypnu repro
        MenuRedraw(DISPLAY_1SEC);       // Ihned prekreslim
        break;
      }
      case K_REDRAW: {
        MenuRedraw(Display);
        break;
      }
      case K_BLINK_ON:
      case K_BLINK_OFF: {
        MenuRedraw(DISPLAY_BLINK);
        break;
      }
    }//switch
  }//while
} // main
