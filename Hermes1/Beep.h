//*****************************************************************************
//
//    Beep.c - Piskani reproduktoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Beep_H__
   #define __Beep_H__

#include "Hardware.h"
#include "..\inc\Pca.h"        // zvuky pomoci PCA

#define BEEP_KEY_DURATION   50          // Delka tonu klaves v milisekundach

void BeepStartup(void);
// Piskne po zapnuti jednotky

void BeepAlarm(void);
// Piskne pri alarmu

void BeepSignal(void);
// Piskne pri vypadku signalu

#endif
